/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Core
 * @{
 *
 * @brief Provides the basic backbone capabilities for the media playback framework, 
 * including functions related to the memory, error code, and format carrier.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 *
 * @since 9
 * @version 1.0
 */

/**
 * @file native_averrors.h
 *
 * @brief Declares the error codes used by the media playback framework.
 *
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVERRORS_H
#define NATIVE_AVERRORS_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the audio and video error codes.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @since 9
 * @version 1.0
 */
typedef enum OH_AVErrCode {
    /**
     * Operation successful.
     */
    AV_ERR_OK = 0,
    /**
     * No memory.
     */
    AV_ERR_NO_MEMORY = 1,
    /**
     * Invalid parameter.
     */
    AV_ERR_OPERATE_NOT_PERMIT = 2,
    /**
     * Invalid value.
     */
    AV_ERR_INVALID_VAL = 3,
    /**
     * I/O error.
     */
    AV_ERR_IO = 4,
    /**
     * Timeout.
     */
    AV_ERR_TIMEOUT = 5,
    /**
     * Unknown error.
     */
    AV_ERR_UNKNOWN = 6,
    /**
     * Unavailable media service.
     */
    AV_ERR_SERVICE_DIED = 7,
    /**
     * Unsupported operation in this state.
     */
    AV_ERR_INVALID_STATE = 8,
    /**
     * Unsupported API.
     */
    AV_ERR_UNSUPPORT = 9,
    /**
     * Initial value for extended error codes.
     */
    AV_ERR_EXTEND_START = 100,
} OH_AVErrCode;

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVERRORS_H
