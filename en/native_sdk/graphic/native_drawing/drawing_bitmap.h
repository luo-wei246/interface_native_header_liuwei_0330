/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_BITMAP_H
#define C_INCLUDE_DRAWING_BITMAP_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module does not provide the pixel unit. The pixel unit to use is consistent with the application context
 * environment. In the ArkUI development environment, the default pixel unit vp is used.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_bitmap.h
 *
 * @brief Declares the functions related to the bitmap in the drawing module.
 *
 * File to include: native_drawing/drawing_bitmap.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines the pixel format of a bitmap, including the color type and alpha type.
 * 
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_BitmapFormat {
    /** Storage format of bitmap pixels. */
    OH_Drawing_ColorFormat colorFormat;
    /** Alpha format of bitmap pixels. */
    OH_Drawing_AlphaFormat alphaFormat;
} OH_Drawing_BitmapFormat;

/**
 * @brief Creates an <b>OH_Drawing_Bitmap</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Bitmap</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_Bitmap* OH_Drawing_BitmapCreate(void);

/**
 * @brief Destroys an <b>OH_Drawing_Bitmap</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_BitmapDestroy(OH_Drawing_Bitmap*);

/**
 * @brief Creates an <b>OH_Drawing_Bitmap</b> object, with the address of the memory for storing the bitmap pixels
 * set to the memory address that you applied for.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Image_Info Pointer to an {@link OH_Drawing_Image_Info} object.
 * @param pixels Pointer to the start address of the memory for storing the bitmap pixels.
 * You need to apply for the memory and ensure its validity.
 * @param rowBytes Size of pixels per row.
 * @return Returns the pointer to the {@link OH_Drawing_Bitmap} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Bitmap* OH_Drawing_BitmapCreateFromPixels(OH_Drawing_Image_Info*, void* pixels, uint32_t rowBytes);

/**
 * @brief Initializes the width and height of a bitmap and sets the pixel format for the bitmap.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @param width Width of the bitmap to be initialized.
 * @param height Height of the bitmap to be initialized.
 * @param OH_Drawing_BitmapFormat Pointer to the pixel format of the bitmap to be initialized,
 * including the pixel color type and alpha type.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_BitmapBuild(
    OH_Drawing_Bitmap*, const uint32_t width, const uint32_t height, const OH_Drawing_BitmapFormat*);

/**
 * @brief Obtains the width of a bitmap.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @return Returns the width.
 * @since 8
 * @version 1.0
 */
uint32_t OH_Drawing_BitmapGetWidth(OH_Drawing_Bitmap*);

/**
 * @brief Obtains the height of a bitmap.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @return Returns the height.
 * @since 8
 * @version 1.0
 */
uint32_t OH_Drawing_BitmapGetHeight(OH_Drawing_Bitmap*);

/**
 * @brief Obtains the pixel address of a bitmap. You can use this address to obtain the pixel data of the bitmap.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @return Returns the pixel address.
 * @since 8
 * @version 1.0
 */
void* OH_Drawing_BitmapGetPixels(OH_Drawing_Bitmap*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
