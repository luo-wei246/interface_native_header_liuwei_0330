/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_FONT_H
#define C_INCLUDE_DRAWING_FONT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module does not provide the pixel unit. The pixel unit to use is consistent with the application context
 * environment. In the ArkUI development environment, the default pixel unit vp is used.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_font.h
 *
 * @brief Declares the functions related to the font in the drawing module.
 *
 * File to include: native_drawing/drawing_font.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Font</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Font</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Font* OH_Drawing_FontCreate(void);

/**
 * @brief Sets the typeface for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param OH_Drawing_Typeface Pointer to an <b>OH_Drawing_Typeface</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTypeface(OH_Drawing_Font*, OH_Drawing_Typeface*);

/**
 * @brief Obtains the typeface of a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @return Returns the pointer to the {@link OH_Drawing_Typeface} object.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_FontGetTypeface(OH_Drawing_Font*);

/**
 * @brief Sets the font size.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param textSize Font size.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSize(OH_Drawing_Font*, float textSize);

/**
 * @brief Obtains the number of glyphs represented by text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param text Pointer to the start address of the storage.
 * @param byteLength Text length, in bytes.
 * @param encoding Encoding type of the text.
 * For details about the available options, see {@link OH_Drawing_TextEncoding}.
 * @since 12
 * @version 1.0
 */
int OH_Drawing_FontCountText(OH_Drawing_Font*, const void* text, size_t byteLength,
    OH_Drawing_TextEncoding encoding);

/**
 * @brief Sets linear scaling for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param isLinearText Whether to set linear scaling.
 * The value <b>true</b> means to set linear scaling, and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetLinearText(OH_Drawing_Font*, bool isLinearText);

/**
 * @brief Sets a horizontal skew factor for a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param skewX Skew of the X axis relative to the Y axis.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSkewX(OH_Drawing_Font*, float skewX);

/**
 * @brief Sets fake bold for a font by increasing the stroke width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param isFakeBoldText Whether to set fake bold.
 * The value <b>true</b> means to set fake bold, and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetFakeBoldText(OH_Drawing_Font*, bool isFakeBoldText);

/**
 * @brief Destroys an <b>OH_Drawing_Font</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontDestroy(OH_Drawing_Font*);

/**
 * @brief Defines a struct that describes the measurement information about a font.
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_Font_Metrics {
    /** Measurement information that is valid. */
    uint32_t fFlags;
    /** Maximum distance from the baseline to the highest coordinate of a character. */
    float top;
    /** Recommended distance from the baseline to the highest coordinate of a character. */
    float ascent;
    /** Recommended distance from the baseline to the lowest coordinate of a character. */
    float descent;
    /** Maximum distance from the baseline to the lowest coordinate of a character. */
    float bottom;
    /** Interline spacing. */
    float leading;
    /** Average character width, or zero if unknown. */
    float avgCharWidth;
    /** Maximum character width, or zero if unknown. */
    float maxCharWidth;
    /**
     * Maximum distance to the leftmost of the font bounding box. Generally, the value is a negative value.
     * Variable fonts are not recommended.
     */
    float xMin;
    /**
     * Maximum distance to the rightmost of the font bounding box. Generally, the value is a negative value.
     * Variable fonts are not recommended.
     */
    float xMax;
    /** Height of a lowercase letter, or zero if unknown. Generally, the value is a negative value. */
    float xHeight;
    /** Height of an uppercase letter, or zero if unknown. Generally, the value is a negative value. */
    float capHeight;
    /** Thickness of the underline. */
    float underlineThickness;
    /**
     * Position of the underline, that is, vertical distance from the baseline to the top of the underline.
     * Generally, the value is a positive value.
     */
    float underlinePosition;
    /** Thickness of the strikethrough. */
    float strikeoutThickness;
    /**
     * Position of the strikethrough, that is, vertical distance from the baseline to the bottom of the strikethrough.
     * Generally, the value is a negative value.
     */
    float strikeoutPosition;
} OH_Drawing_Font_Metrics;

/**
 * @brief Obtains the measurement information about a font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param OH_Drawing_Font_Metrics Pointer to an {@link OH_Drawing_Font_Metrics} object.
 * @return Returns a floating-point variable that indicates the recommended interline spacing.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetMetrics(OH_Drawing_Font*, OH_Drawing_Font_Metrics*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
