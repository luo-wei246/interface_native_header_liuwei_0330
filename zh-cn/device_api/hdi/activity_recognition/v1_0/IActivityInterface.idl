/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiActivityRecognition
 * @{
 *
 * @brief 提供订阅和获取用户行为的API
 *
 * MSDP（Multimodal Sensor Data Platform）可以获取行为识别驱动程序的对象或代理，然后调用该对象或代理提供的API，
 * 获取设备支持的行为类型，订阅或取消订阅不同的行为事件，获取当前的行为事件，以及获取设备缓存的行为事件。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IActivityInterface.idl
 *
 * @brief 声明行为识别模块提供的API，用于获取设备支持的行为类型，订阅或取消订阅不同的行为事件，获取当前的行为事件，以及获取设备缓存的行为事件。
 * 
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief 行为识别模块接口的包路径。
 *
 * @since 3.2
 */
package ohos.hdi.activity_recognition.v1_0;

import ohos.hdi.activity_recognition.v1_0.ActivityRecognitionTypes;
import ohos.hdi.activity_recognition.v1_0.IActivityChangedCallback;

/**
 * @brief 定义对行为识别进行基本操作的接口。
 *
 * 接口包含注册回调函数，取消注册回调函数，获取设备支持的行为类型，获取当前设备的行为类型，订阅和取消订阅行为事件，获取设备缓存的行为事件。
 */
interface IActivityInterface {
    /**
     * @brief 注册回调函数。
     *
     * 用户在获得订阅的行为事件或获取设备缓存的行为事件前，需要先注册该回调函数。数据会通过回调函数进行上报。
     *
     * @param callbackObj 要注册的回调函数，只需成功订阅一次，无需重复订阅。详见{@link IActivityChangedCallback}。
     *
     * @return 如果注册回调函数成功，则返回0。
     * @return 如果注册回调函数失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    RegisterActRecognitionCallback([in] IActivityChangedCallback callbackObj);

    /**
     * @brief 取消注册回调函数。
     *
     * 取消之前注册的回调函数。当不需要使用行为识别功能，或需要更换回调函数时，需要取消注册回调函数。
     *
     * @param callbackObj 要取消注册的回调函数，只需成功取消订阅一次，无需重复取消订阅。详见{@link IActivityChangedCallback}。
     *
     * @return 如果取消注册回调函数成功，则返回0。
     * @return 如果取消注册回调函数失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    UnregisterActRecognitionCallback([in] IActivityChangedCallback callbackObj);

    /**
     * @brief 获取设备支持的行为类型。
     *
     * @param activity 参数类型为字符串，输出设备支持的行为类型。
     * 包括："inVehicle" 、"onBicycle"、"walking"、"running"、"still"、"fast_walking"、"high_speed_rail"
     * "unknown"、"elevator"、"relative_still"、"walking_handhold"、"lying_posture"、"plane"、"metro"等。对应{@link ActRecognitionEventType}中的类型。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetSupportActivity([out] String[] activity);

    /**
     * @brief 获取当前的行为事件。
     *
     * 在调用该接口前，必须先要调用{@Link EnableActRecognitionEvent}接口对行为进行使能。
     *
     * @param event 返回当前设备的行为事件，可能有多个行为共存，详见{@Link ActRecognitionEvent}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetCurrentActivity([out] struct ActRecognitionEvent[] event);

    /**
     * @brief 订阅某个行为事件。
     *
     * 订阅某个行为事件后，若该行为事件有发生，则会在一定时间内上报。
     *
     * @param activity 订阅的行为，通过{@Link GetSupportActivity}得到设备支持的所有行为，然后将行为列表中需要订阅的行为下标作为参数填充。
     * @param eventType 事件类型，参考{@Link ActRecognitionEventType}定义。可以填充1（进入）或2（退出），也可以填充3（同时订阅进入和退出）。
     * @param maxReportLatencyNs 最大上报时间间隔，单位是纳秒。若该时间间隔内有订阅的行为事件发生，则会上报。若存在多个订阅的行为，取最小的时间间隔。
     * @param powerMode 功耗模式。参考{@Link ActRecognitionPowerMode}定义。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    EnableActRecognitionEvent([in] int activity, [in] int eventType, [in] long maxReportLatencyNs, [in] int powerMode);

    /**
     * @brief 取消订阅某个行为事件。
     *
     * 取消订阅某个之前订阅过的行为事件。
     *
     * @param activity 取消订阅的行为，参考{@link EnableActRecognitionEvent}接口的activity参数。
     * @param eventType 事件类型，参考{@Link ActRecognitionEventType}定义。可以填充1（进入）或2（退出），也可以填充3（同时订阅进入和退出）。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    DisableActRecognitionEvent([in] int activity, [in] int eventType);
	
    /**
     * @brief 数据刷新。
     *
     * 刷新设备缓存的所有行为事件并上报。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    FlushActivity();
}
/** @} */