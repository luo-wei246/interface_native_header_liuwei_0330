/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @添加到组 WLAN
 * @{
 *
 * @brief 定义上层WLAN服务的API接口.
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点
 * WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 3.2
 * @version 1.1
 */

 /**
 * @file WlanTypes.idl
 *
 * @brief 定义与WLAN模块相关的数据类型.
 *
 * WLAN模块数据包括{@code-Feature}对象信息、站（STA）信息等,扫描信息和网络设备信息
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @brief 定义WLAN模块接口的包路径.
 *
 * @since 3.2
 * @version 1.1
 */
package ohos.hdi.wlan.v1_1;

/**
 * @brief 定义{@code Feature}对象信息.
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfFeatureInfo {
    /** NIC name of the {@code Feature} object. */
    String ifName;
    /** {@code Feature} object. */
    int type;
};

/**
 * @brief 定义STA信息.
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfStaInfo {
    /** MAC address of an STA. */
    unsigned char[] mac;
};

/**
 * @brief 定义Wi-Fi扫描的服务集标识符（SSID）信息.
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfWifiDriverScanSsid {
    /** SSID to scan. */
    String ssid;
    /** Length of the SSID. */
    int ssidLen;
};

/**
 * @brief 定义Wi-Fi扫描参数.
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfWifiScan{
    /** SSIDs to scan. */
    struct HdfWifiDriverScanSsid[] ssids;
    /** Frequencies to scan. */
    int[] freqs;
    /** Extended information element (IE) carried in a Wi-Fi scan request. */
    unsigned char[] extraIes;
    /** Basic service set identifier (BSSID) to scan. */
    unsigned char[] bssid;
    /** Whether the SSID to be scanned has a prefix. */
    unsigned char prefixSsidScanFlag;
    /** Fast connect flag. */
    unsigned char fastConnectFlag;
};

/**
 * @brief 定义网络设备信息.
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfNetDeviceInfo {
    /** Index of the network device. */
    unsigned int index;
    /** Network interface card (NIC) name. */
    String ifName;
    /** Length of the NIC name. */
    unsigned int ifNameLen;
    /** NIC type. */
    unsigned char iftype;
    /** MAC address of the network device. */
    unsigned char[] mac;
};

/**
 * @brief 定义网络设备信息集.
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfNetDeviceInfoResult {
    /** A sef of network device information. */
    struct HdfNetDeviceInfo[] deviceInfos;
};

/**
 * @brief 定义Wi-Fi扫描结果.
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfWifiScanResult {
    /** Flag of the basic service set (BSS)/independent basic service set (IBSS). */
    unsigned int flags;
    /** BSSID information. */
    unsigned char[] bssid;
    /** Capability information fields (in host byte order). */
    unsigned short caps;
    /** Channel frequency. */
    unsigned int freq;
    /** Beacon interval. */
    unsigned short beaconInt;
    /** Signal quality. */
    int qual;
    /** Signal strength. */
    int level;
    /** Time for receiving the latest beacon or probe response frame, in milliseconds. */
    unsigned int age;
    /** Variable value in the scan result. */
    unsigned char[] variable;
    /** IE in the following Probe Response message. */
    unsigned char[] ie;
    /** IE in the following beacon. */
    unsigned char[] beaconIe;
};

/**
 * @brief 定义Wi-Fi扫描结果.
 *
 * @since 4.0
 * @version 1.1
 */
struct HdfWifiScanResultExt {
    /** Flag of the basic service set (BSS)/independent basic service set (IBSS). */
    unsigned int flags;
    /** BSSID information. */
    unsigned char[] bssid;
    /** Capability information fields (in host byte order). */
    unsigned short caps;
    /** Channel frequency. */
    unsigned int freq;
    /** Beacon interval. */
    unsigned short beaconInt;
    /** Signal quality. */
    int qual;
    /** Signal strength. */
    int level;
    /** Time for receiving the latest beacon or probe response frame, in milliseconds. */
    unsigned int age;
    /** Timestamp **/
    unsigned long tsf;
    /** Variable value in the scan result. */
    unsigned char[] variable;
    /** IE in the following Probe Response message. */
    unsigned char[] ie;
    /** IE in the following beacon. */
    unsigned char[] beaconIe;
};

/**
 * @brief 定义Wi-Fi扫描结果.
 *
 * @since 4.0
 * @version 1.1
 */
struct HdfWifiScanResults {
    struct HdfWifiScanResultExt[] res;
};

/**
 * @brief 定义Wi-Fi频带信息.
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfWifiInfo {
    /** Wi-Fi frequency band. */
    int band;
    /** Number of frequencies supported in the Wi-Fi frequency band. */
    unsigned int size;
};

/**
 * @brief 定义通道测量参数.
 *
 * @since 3.2
 * @version 1.1
 */
struct MeasChannelParam {
    /** Channel ID. */
    int channelId;
    /** Measure time. */
    int measTime;
};

/**
 * @brief 定义通道测量结果.
 *
 * @since 3.2
 * @version 1.1
 */
struct MeasChannelResult {
    /** Channel ID. */
    int channelId;
    /** Channel load. */
    int chload;
    /** Channel noise. */
    int noise;
};

/**
 * @brief 定义投影参数.
 *
 * @since 3.2
 * @version 1.1
 */
struct ProjectionScreenCmdParam {
    /** ID of the projection command. */
    int cmdId;
    /** Content of the projection command. */
    byte[] buf;
};

/**
 * @brief 定义STA信息.
 *
 * @since 3.2
 * @version 1.1
 */
struct WifiStationInfo {
    /** Receive (RX) rate. */
    unsigned int rxRate;
    /** Transmit (TX) rate. */
    unsigned int txRate;
    /** Transmission type. */
    unsigned int flags;
    /** RX Very High Throughput Modulation and Coding Scheme (VHT-MCS) configuration. */
    unsigned char rxVhtmcs;
    /** TX VHT-MCS configuration. */
    unsigned char txVhtmcs;
    /** RX Modulation and Coding Scheme (MCS) index. */
    unsigned char rxMcs;
    /** TX MCS index. */
    unsigned char txMcs;
    /** RX Very High Throughput Number of Spatial Streams (VHT-NSS) configuration. */
    unsigned char rxVhtNss;
    /** TX VHT-NSS configuration. */
    unsigned char txVhtNss;
};

struct AdjustChannelInfo {
    int msgId;
    unsigned char chanNumber;
    unsigned char bandwidth;
    unsigned char switchType;
    unsigned char statusCode;
};

/**
 * @brief 定义Pno扫描网络信息.
 *
 * @since 4.0
 * @version 1.1
 */
struct PnoNetwork {
    /** whether to scan hidden network. */
    boolean isHidden;
    /** Frequencies to scan. */
    int[] freqs;
    /** Ssid to scan. */
    struct HdfWifiDriverScanSsid ssid;
};

/**
 * @brief 定义Pno扫描参数.
 *
 * @since 4.0
 * @version 1.1
 */
struct PnoSettings {
    /** Minimum 2G Rssi to scan. */
    int min2gRssi;
    /** Minimum 5G Rssi to scan. */
    int min5gRssi;
    /** Scan interval. */
    int scanIntervalMs;
    /** Scan iteration. */
    int scanIterations;
    /** Scan network list. */
    struct PnoNetwork[] pnoNetworks;
};

/**
 * @brief 定义信号轮询信息.
 *
 * @since 4.0
 * @version 1.1
 */
struct SignalPollResult {
    /** Rssi value in dBM. */
    int currentRssi;
    /** Association frequency in MHz. */
    int associatedFreq;
    /** Transmission bit rate in Mbps. */
    int txBitrate;
    /** Received bit rate in Mbps. */
    int rxBitrate;
    /** Noise value in dBM. */
    int currentNoise;
    /** Snr value in dB. */
    int currentSnr;
    /** Current channel load. */
    int currentChload;
    /** Uldelay value in ms. */
    int currentUlDelay;
    /** TxBytes value. */
    int currentTxBytes;
    /** RxBytes value. */
    int currentRxBytes;
    /** TxFailed value. */
    int currentTxFailed;
    /** TxPackets value. */
    int currentTxPackets;
    /** RxPackets value. */
    int currentRxPackets;
};
/** @} */