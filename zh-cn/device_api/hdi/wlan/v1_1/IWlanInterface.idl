/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @添加到组 WLAN
 * @{
 *
 * @brief 定义上层WLAN服务的API接口.
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点
 * WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @file IWlanInterface.idl
 *
 * @brief 提供API以启用或禁用WLAN热点、扫描热点、连接到WLAN热点或断开与WLAN热点的连接,设置国家代码,并管理网络设备.
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @brief 定义WLAN模块接口的包路径.
 *
 * @since 3.2
 * @version 1.1
 */
package ohos.hdi.wlan.v1_1;

import ohos.hdi.wlan.v1_1.WlanTypes;
import ohos.hdi.wlan.v1_1.IWlanCallback;

/**
 * @brief 定义上层WLAN服务的接口.
 *
 * @since 3.2
 * @version 1.1
 */

interface IWlanInterface {
    /**
     * @brief 在HAL和WLAN驱动程序之间创建一个通道,并获取驱动程序网络接口卡（NIC）信息,此函数必须在创建IWiFi实例后调用
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    Start();

    /**
     * @brief 销毁HAL和WLAN驱动程序之间的通道。此函数必须在IWiFi实例被销毁之前调用.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    Stop();

    /**
     * @brief 基于指定的类型创建Feature对象.
     *
     * @param type 表示要创建的Feature对象的类型, 2:Station, 3:AP.
     * @param ifeature 表示创建feature对象.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    CreateFeature([in] int type, [out] struct HdfFeatureInfo ifeature);

    /**
     * @brief 销毁Feature对象.
     *
     * @param ifeature 表示要销毁的Feature对象.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    DestroyFeature([in] struct HdfFeatureInfo ifeature);

    /**
     * @brief 获取连接到此AP的所有STA的信息。目前，STA信息仅包含MAC地址。
     *
     * @param ifeature 表示Feature对象.
     * @param staInfo 表示所有连接到AP的STA的基本信息.
     * @param num 表示连接的STA的数量.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetAssociatedStas([in] struct HdfFeatureInfo ifeature, [out] struct HdfStaInfo[] staInfo, [out] unsigned int num);

    /**
     * @brief 获取当前驱动程序的芯片ID.
     *
     * @param ifeature 表示Feature对象.
     * @param chipId 表示获得的芯片ID.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetChipId([in] struct HdfFeatureInfo ifeature, [out] unsigned char chipId);

    /**
     * @brief 获取设备MAC地址.
     *
     * @param ifeature 表示Feature对象.
     * @param mac 表示获得的MAC地址.
     * @param len 表示MAC地址的长度,该值固定为6.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetDeviceMacAddress([in] struct HdfFeatureInfo ifeature, [out] unsigned char[] mac, [in] unsigned char len);

    /**
     * @brief 基于指定的NIC名称获取Feature对象.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param ifeature 表示获得的Feature对象.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetFeatureByIfName([in] String ifName, [out] struct HdfFeatureInfo ifeature);

    /**
     * @brief 获取Feature对象的类型.
     *
     * @param ifeature 表示Feature对象.
     * @param featureType 表示获取的Feature对象的类型.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetFeatureType([in] struct HdfFeatureInfo ifeature, [out] int featureType);

    /**
     * @brief 获得指定频段支持的频率.
     *
     * @param ifeature 表示Feature对象.
     * @param wifiInfo 表示频率信息,wifiInfo.band, 0:2.4 GHz; 1:5 GHz, wifiInfo.size,最小为14。
     * @param freq Indicates the supported frequencies obtained.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetFreqsWithBand([in] struct HdfFeatureInfo ifeature, [in] struct HdfWifiInfo wifiInfo, [out] int[] freq);

    /**
     * @brief 获取芯片的所有NIC名称.
     *
     * @param chipId 表示目标芯片的ID.
     * @param ifNames 表示获得的NIC名称.
     * @param num 表示NIC的数量.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetIfNamesByChipId([in] unsigned char chipId, [out] String ifName, [out] unsigned int num);

    /**
     * @brief 获取基于Feature对象的NIC名称.
     *
     * @param ifeature 表示Feature对象.
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetNetworkIfaceName([in] struct HdfFeatureInfo ifeature, [out] String ifName);

    /**
     * @brief 获取多个NIC共存的信息.
     *
     * @param combo 表示获得的信息,例如,AP、STA和P2P的不同组合.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetSupportCombo([out] unsigned long combo);

    /**
     * @brief 获得设备支持的WLAN功能，无论设备状态如何.
     *
     * @param supType 表示获得的功能.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetSupportFeature([out] unsigned char[] supType);

    /**
     * @brief 注册回调以侦听异步事件.
     *
     * @param cbFunc 表示要注册的回调.
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    RegisterEventCallback([in] IWlanCallback cbFunc, [in] String ifName);

    /**
     * @brief 注销回调.
     *
     * @param cbFunc 表示要注销的回调.
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    UnregisterEventCallback([in] IWlanCallback cbFunc, [in] String ifName);

    /**
     * @brief 根据指定的芯片ID重新启动WLAN驱动程序.
     *
     * @param chipId 表示要重新启动其驱动程序的芯片的ID.
     * @param ifName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    ResetDriver([in] unsigned char chipId, [in] String ifName);

    /**
     * @brief 设置国家码.
     *
     * 国家/地区代码表示AP射频所在的国家/地区。描述AP射频特性，
     * 包括AP的发射功率和支持的信道，确保AP的射频属性符合当地法律法规
     *
     * @param ifeature 表示Feature对象.
     * @param code 表示设置的国家码.
     * @param len 表示国家码的长度.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    SetCountryCode([in] struct HdfFeatureInfo ifeature, [in] String code, [in] unsigned int len);

    /**
     * @brief 设置NIC的MAC地址.
     *
     * @param ifeature 表示Feature对象.
     * @param mac 表示要设置的MAC地址.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    SetMacAddress([in] struct HdfFeatureInfo ifeature, [in] unsigned char[] mac);

    /**
     * @brief 扫描单个MAC地址.
     *
     * @param ifeature 表示Feature对象.
     * @param scanMac 表示STA要扫描的MAC地址.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    SetScanningMacAddress([in] struct HdfFeatureInfo ifeature, [in] unsigned char[] scanMac);

    /**
     * @brief 设置发射功率.
     *
     * @param ifeature 表示Feature对象.
     * @param power 表示要设置的发射功率.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    SetTxPower([in] struct HdfFeatureInfo ifeature, [in] int power);

    /**
     * @brief 获取网络设备信息，如设备索引、NIC名称和MAC地址.
     *
     * @param netDeviceInfoResult 表示获得的网络设备信息.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetNetDevInfo([out] struct HdfNetDeviceInfoResult netDeviceInfoResult);

    /**
     * @brief 开始扫描.
     *
     * @param ifeature 表示Feature对象.
     * @param scan 表示扫描参数.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    StartScan([in] struct HdfFeatureInfo ifeature, [in] struct HdfWifiScan scan);

    /**
     * @brief 获得使用中的电源模式.
     *
     * @param ifeature 表示Feature对象.
     * @param mode 表示获得的电源模式。电源模式可以是睡眠(在待机模式下运行),一般(以正常额定功率运行),
     * 和穿墙(以最大功率运行以提高信号强度和覆盖范围)。
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetPowerMode([in] struct HdfFeatureInfo ifeature, [out] unsigned char mode);

    /**
     * @brief 设置电源模式.
     *
     * @param ifeature 表示Feature对象.
     * @param mode 表示设置电源模式。电源模式可以是睡眠(在待机模式下运行),一般(以正常额定功率运行),
     * 和穿墙(以最大功率运行以提高信号强度和覆盖范围)。
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    SetPowerMode([in] struct HdfFeatureInfo ifeature, [in] unsigned char mode);

    /**
     * @brief 开始通道测量.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param measChannelParam 表示通道测量参数（通道ID和测量时间）.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    [oneway] StartChannelMeas([in] String ifName, [in] struct MeasChannelParam measChannelParam);

    /**
     * @brief 获得通道测量结果.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param measChannelResult 指示通道测量结果(包括通道ID、负载和噪声)
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetChannelMeasResult([in] String ifName, [out] struct MeasChannelResult measChannelResult);

    /**
     * @brief 设置投影参数.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param param 表示要设置的投影参数.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    SetProjectionScreenParam([in] String ifName, [in] struct ProjectionScreenCmdParam param);

    /**
     * @brief 向驱动程序发送I/O控制命令.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param cmdId 表示要发送的命令的ID.
     * @param paramBuf 表示命令内容.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    WifiSendCmdIoctl([in] String ifName, [in] int cmdId, [in] byte[] paramBuf);

    /**
     * @brief 获取指定NIC的STA信息.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param info 表示所获得的STA信息。有关详细信息，请参阅{@link WifiStationInfo}.
     * @param mac 表示STA的MAC地址.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 3.2
     * @version 1.1
     */
    GetStaInfo([in] String ifName, [out] struct WifiStationInfo info, [in] unsigned char[] mac);

    /**
     * @brief 启动Pno扫描.
     *
     * @param interfaceName 表示网卡(NIC)名称.
     * @param pnoSettings 表示pno扫描参数.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.0
     * @version 1.1
     */
    StartPnoScan([in] String interfaceName, [in] struct PnoSettings pnoSettings);

    /**
     * @brief 关闭Pno扫描.
     *
     * @param interfaceName 表示网卡(NIC)名称.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.0
     * @version 1.1
     */
    StopPnoScan([in] String interfaceName);

    /**
     * @brief 获取相关链路的信号信息。此函数必须在STA模式下调用.
     *
     * @param ifName 表示网卡(NIC)名称.
     * @param signalResult 表示信号信息.
     *
     * @return 返回值 如果操作成功，则返回0.
     * @return 返回值 如果操作失败，则为负值.
     *
     * @since 4.0
     * @version 1.1
     */
    GetSignalPollInfo([in] String ifName, [out] struct SignalPollResult signalResult);
}
/** @} */
