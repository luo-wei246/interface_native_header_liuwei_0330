/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Audio模块接口定义
 *
 * 音频接口涉及自定义类型、驱动加载接口、驱动适配器接口、音频播放（render）接口、音频录音（capture）接口等
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file audio_attribute.h
 *
 * @brief Audio属性的接口定义文件
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef AUDIO_ATTRIBUTE_H
#define AUDIO_ATTRIBUTE_H

#include "audio_types.h"

/**
 * @brief AudioAttribute音频属性接口
 *
 * 提供音频播放（render）或录音（capture）需要的公共属性驱动能力，包括获取帧（frame）信息、设置采样属性等
 *
 * @since 1.0
 * @version 1.0
 */

struct AudioAttribute {
    /**
     * @brief 获取音频帧（frame）的大小
     *
     * 获取一帧音频数据的长度（字节数）
     *
     * @param handle 待操作的音频句柄
     * @param size 获取的音频帧大小（字节数）保存到size中
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*GetFrameSize)(AudioHandle handle, uint64_t *size);

    /**
     * @brief 获取音频buffer中的音频帧数
     *
     * @param handle 待操作的音频句柄
     * @param count 一个音频buffer中包含的音频帧数，获取后保存到count中
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*GetFrameCount)(AudioHandle handle, uint64_t *count);

    /**
     * @brief 设置音频采样的属性参数
     *
     * @param handle 待操作的音频句柄
     * @param attrs 待设置的音频采样属性，例如采样频率、采样精度、通道
     * @return 成功返回值0，失败返回负值
     * @see GetSampleAttributes
     */
    int32_t (*SetSampleAttributes)(AudioHandle handle, const struct AudioSampleAttributes *attrs);

    /**
     * @brief 获取音频采样的属性参数
     *
     * @param handle 待操作的音频句柄
     * @param attrs 获取的音频采样属性（例如采样频率、采样精度、通道）保存到attrs中
     * @return 成功返回值0，失败返回负值
     * @see SetSampleAttributes
     */
    int32_t (*GetSampleAttributes)(AudioHandle handle, struct AudioSampleAttributes *attrs);

    /**
     * @brief 获取音频的数据通道ID
     *
     * @param handle 待操作的音频句柄
     * @param channelId 获取的通道ID保存到channelId中
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*GetCurrentChannelId)(AudioHandle handle, uint32_t *channelId);

    /**
     * @brief 设置音频拓展参数
     *
     * @param handle 待操作的音频句柄
     * @param keyValueList 拓展参数键值对字符串列表，格式为key=value，多个键值对通过分号分割
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*SetExtraParams)(AudioHandle handle, const char *keyValueList);

    /**
     * @brief 获取音频拓展参数
     *
     * @param handle 待操作的音频句柄
     * @param keyValueList 拓展参数键值对字符串列表，格式为key=value，多个键值对通过分号分割
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*GetExtraParams)(AudioHandle handle, char *keyValueList);

    /**
     * @brief 请求mmap缓冲区
     *
     * @param handle 待操作的音频句柄
     * @param reqSize 请求缓冲区的大小
     * @param desc 缓冲区描述符
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*ReqMmapBuffer)(AudioHandle handle, int32_t reqSize, struct AudioMmapBufferDescripter *desc);

    /**
     * @brief 获取当前mmap的读/写位置
     *
     * @param handle 待操作的音频句柄
     * @param frames 获取的音频帧计数保存到frames中
     * @param time 获取的关联时间戳保存到time中
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*GetMmapPosition)(AudioHandle handle, uint64_t *frames, struct AudioTimeStamp *time);
};

#endif /* AUDIO_ATTRIBUTE_H */
/** @} */
