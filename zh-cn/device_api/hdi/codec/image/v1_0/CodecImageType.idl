/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Codec
 * @{
 *
 * @brief 定义图像编解码器模块的接口
 *
 * 编解码器模块提供用于图像编解码、设置编解码器参数以及控制和传输图像数据的接口。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file CodecImageTypes.idl
 *
 * @brief 定义图像编解码器模块API中使用的自定义数据类型，包括编解码器图像参数、类型和缓冲区。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @brief 定义图像编解码器模块API的包的路径。
 *
 * @since 4.0
 * @version 1.0
 */
package ohos.hdi.codec.image.v1_0;

/**
 * @brief 定义图像区域信息。
 */
struct CodecImageRegion {
    unsigned int left;      /**< 到图像左侧的距离。 */
    unsigned int right;     /**< 到图像右侧的距离。 */
    unsigned int top;       /**< 到图像顶部的距离。 */
    unsigned int bottom;    /**< 到图像底部的距离。 */
    unsigned int flag;      /**< 区域解码开关 */
    unsigned int rsv;       /**< 扩展预留 */
};

/**
 * @brief 编解码的图像格式枚举
 */
enum CodecImageRole {
    CODEC_IMAGE_JPEG = 0,       /**< Jpeg格式. */
    CODEC_IMAGE_HEIF,           /**< Heif格式. */
    CODEC_IMAGE_INVALID,        /**< 无效的格式 */
};

/**
 * @brief 定义编解码图像缓冲区信息。
 */
struct CodecImageBuffer {
    unsigned int id;            /**< 缓冲区ID. */
    unsigned int size;          /**< 缓冲区大小 */
    NativeBuffer buffer;        /**< 用于编码或解码的缓冲区句柄。详见{@link NativeBuffer}. */
    FileDescriptor fenceFd;     /**< Fence文件描述符*/
    CodecImageRole bufferRole;  /**< 图像编码格式。详见{@link CodecImageRole}. */
};

/**
 * @brief 定义图像编解码器类型。
 */
enum CodecImageType {
    CODEC_IMAGE_TYPE_DECODER = 0,     /**< 图像解码器。 */
    CODEC_IMAGE_TYPE_ENCODER,         /**< 图像编码器。 */
    CODEC_IMAGE_TYPE_INVALID,         /**< 无效类型 */
};

/**
 * @brief 定义图像编解码器功能。
 */
struct CodecImageCapability {
    String name;                        /**< 图像编解码器的名称。 */
    enum CodecImageRole role;           /**< 图像编解码器的格式。 */
    enum CodecImageType type;           /**< 图像编解码器的类型。 */
    unsigned int widthAlignment;        /**< 宽度的对齐值。 */
    unsigned int heightAlignment;       /**< 高度的对齐值。 */
    unsigned int maxSample;             /**< 最大采样值 */
    unsigned int maxWidth;              /**< 最大宽度 */
    unsigned int maxHeight;             /**< 最大高度 */
    unsigned int minWidth;              /**< 最小宽度 */
    unsigned int minHeight;             /**< 最小高度 */
    unsigned int maxInst;               /**< 最大实例数. */
    unsigned int[] supportPixFmts;      /**< 支持PixFormat. 详见{@link PixFormat}. */
    boolean isSoftwareCodec;            /**< 是否软件编解码 */
};

/**
 * @brief 定义jpeg图像量化表信息。
 */
struct CodecJpegQuantTable {
    unsigned short[] quantVal;          /**< Quant表值。*/
    boolean tableFlag;                  /**< quant表有效时为True。*/
};

/**
 * @brief 定义jpeg图像Huffman表信息。
 */
struct CodecJpegHuffTable {
    unsigned char[] bits;               /**< 比特值, bits[0]未使用 */
    unsigned char[] huffVal;            /**< Huffman表值*/
    boolean tableFlag;                  /**< Huffman表有效时为true */
};

/**
 * @brief 定义jpeg图像的颜色分量信息。
 */
struct CodecJpegCompInfo {
    unsigned int componentId;           /**< 颜色分量ID */
    unsigned int componentIndex;        /**< 颜色分量索引 */
    unsigned int hSampFactor;           /**< 水平采样因子 */
    unsigned int vSampFactor;           /**< 垂直采样因子 */
    unsigned int quantTableNo;          /**< Quant表值 */
    unsigned int dcTableNo;             /**< Dc表索引 */
    unsigned int acTableNo;             /**< Ac表索引 */
    boolean infoFlag;
};

/**
 * @brief 定义jpeg图像解码信息。
 */
struct CodecJpegDecInfo {
    unsigned int imageWidth;                        /**< 图像宽度 */
    unsigned int imageHeight;                       /**< 图像高度 */
    unsigned int dataPrecision;                     /**< 像素位宽 */
    unsigned int numComponents;                     /**< jpeg图像中颜色分量的数量。*/
    unsigned int restartInterval;                   /**< 在一次扫描中作为独立序列处理的MCU的个数 */
    boolean arithCode;                              /**< Huffman编码为false，arithmetic编码为true */
    boolean progressiveMode;                        /**< 是否SOF指定渐进模式。 */
    struct CodecJpegCompInfo[] compInfo;            /**< Jpeg压缩信息。 */
    struct CodecJpegHuffTable[] dcHuffTbl;          /**< Dc huffman表信息 */
    struct CodecJpegHuffTable[] acHuffTbl;          /**< Ac huffman表信息 */
    struct CodecJpegQuantTable[] quantTbl;          /**< Quant表信息 */
    struct CodecImageRegion region;                 /**< 图像区域信息。*/
    unsigned int sampleSize;                        /**< 图像样本大小 */
    unsigned int compressPos;                       /**< Jpeg压缩数据的偏移量。*/
};
