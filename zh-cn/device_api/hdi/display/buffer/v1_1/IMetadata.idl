/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 4.1
 * @version 1.1
 */

/**
 * @file IMetadata.idl
 *
 * @brief 显示数据映射接口声明。
 *
 * @since 4.1
 * @version 1.1
 */

/**
 * @brief Display模块接口的包路径。
 *
 * @since 4.1
 * @version 1.1
 */
package ohos.hdi.display.buffer.v1_1;

interface IMetadata {
    /**
     * @brief IPC后的初始化NativeBuffer
     *
     * @param handle 待无效cache的handle指针。
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    RegisterBuffer([in] NativeBuffer handle);

    /**
     * @brief 通过键值对的方式设置随帧数据
     *
     * @param handle 待无效cache的handle指针。
     * @param key  数据键
     * @param value 数据值
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    SetMetadata([in] NativeBuffer handle, [in] unsigned int key, [in] unsigned char[] value);

    /**
     * @brief 通过键值对的方式设置随帧数据
     *
     * @param handle 待无效cache的handle指针。
     * @param key metadata key
     * @param value metadata value
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    GetMetadata([in] NativeBuffer handle, [in] unsigned int key, [out] unsigned char[] value);

    /**
     * @brief 列出bufferhandle中设置的所有key值
     *
     * @param handle 待无效cache的handle指针。
     * @param keys 数据键
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    ListMetadataKeys([in] NativeBuffer handle, [out] unsigned int[] keys);

    /**
     * @brief 按键值内存删除数据
     *
     * @param handle 待无效cache的handle指针。
     * @param key 要擦除的元数据密钥
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    EraseMetadataKey([in] NativeBuffer handle, [in] unsigned int key);
}
/** @} */
