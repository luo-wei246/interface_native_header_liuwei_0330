/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiSecureElement
 * @{
 *
 * @brief 为SecureElement服务提供统一的访问安全单元的接口
 * 
 * SecureElement服务通过获取SecureElementInterface对象提供的API接口访问安全单元，包括初始化、
 * 判断安全单元状态、创建关闭逻辑通道、与SE进行APDU指令交互等
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file ISecureElementCallback.idl
 *
 * @brief 定义回调的接口文件
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.secure_element.v1_0;

/**
 * @file ISecureElementCallback.idl
 *
 * @brief 声明从SecureElement HDF到SecureElement服务的报告状态回调。
 *
 * @since 4.0
 * @version 1.0
 */
[callback] interface ISecureElementCallback {
    /**
    * @brief 通知SE状态已更改。
    * @param connected 表示SE是否已连接。
    *
    * @since 4.0
    */
    OnSeStateChanged([in] boolean connected);
}
/** @} */