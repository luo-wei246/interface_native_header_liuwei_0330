/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MemoryTracker
 * @{
 *
 * @brief 实现对设备（如GPU）内存占用的统一查询，如GPU占用的GL和Graphic内存等。
 *
 * 需要查询GPU等外设内存占用时使用，例如hidumper中使用本模块IMemoryTrackerInterface接口列出每个进程的GPU内存占用。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file MemoryTrackerTypes.idl
 *
 * @brief 设备内存跟踪模块中使用的数据类型，包括内存类型、内存类型标记、设备内存信息。
 *
 * 需要查询外设内存占用时使用，定义的内存类型、内存类型标记、内存设备信息配合模块中其他部分使用。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief 设备内存跟踪接口的包路径
 *
 * @since 3.2
 */
package ohos.hdi.memorytracker.v1_0;

/**
 * @brief 内存类型
 *
 * @since 3.2
 */
enum MemoryTrackerType {
    /** 多媒体相关 */
    MEMORY_TRACKER_TYPE_MM = 0,
    /** GL相关 */
    MEMORY_TRACKER_TYPE_GL = 1,
    /** 相机相关 */
    MEMORY_TRACKER_TYPE_CAM = 2,
    /** 图形相关 */
    MEMORY_TRACKER_TYPE_GRAPH = 3,
    /** 其他 */
    MEMORY_TRACKER_TYPE_OTHER = 4,
    MEMORY_TRACKER_TYPE_COUNTS,
};

/**
 * @brief 内存类型标记
 *
 * @since 3.2
 */
enum MemoryTrackerFlag {
    /** 与其他进程共享内存 */
    FLAG_SHARED_RSS = 2,
    /** 与其他进程共享内存 / 共享内存计数 */
    FLAG_SHARED_PSS = 4,
    /** 不与其他进程共享内存 */
    FLAG_PRIVATE = 8,
    /** 内存映射到smaps中 */
    FLAG_MAPPED = 16,
    /** 内存不映射到smaps中 */
    FLAG_UNMAPPED = 32,
    /** CPU安全模式相关 */
    FLAG_PROTECTED = 64,
    /** CPU安全模式无关 */
    FLAG_UNPROTECTED = 128,
    /** 系统管理内存 */
    FLAG_SYSTEM = 256,
    /** 系统管理例外情况 */
    FLAG_SYSTEM_EXCEPT = 512,

};

/**
 * @brief 设备内存信息
 *
 * @since 3.2
 */
struct MemoryRecord {
    /** 内存类型标记，可选类型请参考{@Link MemoryTrackerFlag} */
    int flags;
    /** 该类型内存大小，以字节为单位 */
    long size;
};
