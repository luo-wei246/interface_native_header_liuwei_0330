/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdfUserAuth
 * @{
 *
 * @brief 提供用户认证驱动的标准API接口。
 *
 * 用户认证驱动为用户认证服务提供统一的访问接口。
 *
 * @since 4.0
 */

 /**
 * @file UserAuthTypes.idl
 *
 * @brief 定义用户认证驱动的枚举类和数据结构。
 *
 * @since 4.0
 */

package ohos.hdi.user_auth.v1_1;

import ohos.hdi.user_auth.v1_0.UserAuthTypes;

/**
 * @brief 调度信息。
 *
 * @since 4.0
 * @version 1.1
 */
struct ScheduleInfoV1_1 {
    /** 调度ID，用于标识一次操作请求的执行器调度过程。 */
    unsigned long scheduleId;
    /** 模版id列表。 */
    unsigned long[] templateIds;
    /** 用户认证凭据类型@{AuthType}。 */
    enum AuthType authType;
    /** 执行器匹配器。 */
    unsigned int executorMatcher;
    /** 调度模式@{scheduleMode}。 */
    unsigned int scheduleMode;
    /** 执行器信息列表@{ExecutorInfo}。 */
    struct ExecutorInfo[] executors;
    /** 其他相关信息，用于支持信息扩展。 */
    unsigned char[] extraInfo;
};

/**
 * @brief 用户信息
 *
 * @since 4.0
 * @version 1.1
 */
struct UserInfo {
    /** 用户的SecureUid。 */
    unsigned long secureUid;
    /** 用户的口令认证子类型@{@PinSubType}。 */
    enum PinSubType pinSubType;
    /** 用户的注册信息列表@{EnrolledInfo}。 */
    struct EnrolledInfo[] enrolledInfos;
};
/** @} */