/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdfUserAuth
 * @{
 *
 * @brief 提供用户认证驱动的标准API接口。
 *
 * 用户认证驱动为用户认证服务提供统一的访问接口。
 *
 * @since 3.2
 */

 /**
 * @file UserAuthTypes.idl
 *
 * @brief 定义用户认证驱动的枚举类和数据结构。
 *
 * @since 3.2
 */

package ohos.hdi.user_auth.v1_0;

 /**
 * @brief 枚举用户认证凭据类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum AuthType : int {
    /** 表示包含所有认证凭据类型。 */
    ALL = 0,
    /** 认证凭据类型为口令。 */
    PIN = 1,
    /** 认证凭据类型为人脸。 */
    FACE = 2,
    /** 认证凭据类型为指纹。 */
    FINGERPRINT = 4,
};

/**
 * @brief 枚举执行器角色。
 *
 * @since 3.2
 * @version 1.0
 */
enum ExecutorRole : int {
    /** 执行器角色为采集器，提供用户认证时的数据采集能力，需要和认证器配合完成用户认证。 */
    COLLECTOR = 1,
    /** 执行器角色为认证器，提供用户认证时数据处理能力，读取存储凭据模板信息并完成比对。 */
    VERIFIER = 2,
    /** 执行器角色为全功能执行器，可提供用户认证数据采集、处理、储存及比对能力。 */
    ALL_IN_ONE = 3,
};

/**
 * @brief 枚举执行器安全等级。
 *
 * @since 3.2
 * @version 1.0
 */
enum ExecutorSecureLevel : int {
    /** 执行器安全级别为0，关键操作在无访问控制执行环境中完成。 */
    ESL0 = 0,
    /** 执行器安全级别为1，关键操作在有访问控制的执行环境中完成。 */
    ESL1 = 1,
    /** 执行器安全级别为2，关键操作在可信执行环境中完成。 */
    ESL2 = 2,
    /** 执行器安全级别为3，关键操作在高安环境如独立安全芯片中完成。 */
    ESL3 = 3,
};

/**
 * @brief 口令认证子类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum PinSubType : int {
    PIN_SIX = 10000, /** 六位口令密码。 */
    PIN_NUMBER = 10001, /** 数字口令密码。 */
    PIN_MIX = 10002, /** 混合密码。 */
};

/**
 * @brief 调度模式。
 *
 * @since 3.2
 * @version 1.0
 */
enum ScheduleMode : int {
    /** 录入模式。 */
    ENROLL = 0,
    /** 认证模式。 */
    AUTH = 1,
    /** 识别模式。 */
    IDENTIFY = 2,
};

/**
 * @brief 执行器注册信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct ExecutorRegisterInfo {
    /** 用户认证凭据类型@{AuthType}。 */
    enum AuthType authType;
    /** 执行器角色@{ExecutorRole}。 */
    enum ExecutorRole executorRole;
    /** 既定用户认证凭据类型的执行器传感器提示，用于找到对应认证方式的传感器。 */
    unsigned int executorSensorHint;
    /** 执行器匹配器，根据执行器支持的认证能力进行分类。 */
    unsigned int executorMatcher;
    /** 执行器安全等级@{ExecutorSecureLevel}。 */
    enum ExecutorSecureLevel esl;
    /** 执行器公钥，用于校验该执行器私钥签名的信息。 */
    unsigned char[] publicKey;
};

/**
 * @brief 执行器信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct ExecutorInfo {
    /** 用户认证框架的执行器索引。 */
    unsigned long executorIndex;
    /** 执行器注册信息@{ExecutorRegisterInfo}。 */
    struct ExecutorRegisterInfo info;
};

/**
 * @brief 调度信息。
 *
 * @since 3.2
 * @version 1.0
 *
 * @deprecated 从4.0版本开始废弃，使用{@link ScheduleInfoV1_1}接口代替。
 */
struct ScheduleInfo {
    /** 调度ID，用于标识一次操作请求的执行器调度过程。 */
    unsigned long scheduleId;
    /** 模版id列表。 */
    unsigned long[] templateIds;
    /** 用户认证凭据类型@{AuthType}。 */
    enum AuthType authType;
    /** 执行器匹配器。 */
    unsigned int executorMatcher;
    /** 调度模式@{scheduleMode}*/
    unsigned int scheduleMode;
    /** 执行器信息列表@{ExecutorInfo}。 */
    struct ExecutorInfo[] executors;
};

/**
 * @brief 认证方案。
 *
 * @since 3.2
 * @version 1.0
 */
struct AuthSolution {
    /** 用户ID。 */
    int userId;
    /** 认证结果可信等级。 */
    unsigned int authTrustLevel;
    /** 用户认证凭据类型@{AuthType}。 */
    enum AuthType authType;
    /** 既定用户认证凭据类型的执行器传感器提示，用于找到对应认证方式的传感器。 */
    unsigned int executorSensorHint;
    /** 挑战值，用于签发认证令牌。 */
    unsigned char[] challenge;
};

/**
 * @brief 执行器发送的消息。
 *
 * @since 3.2
 * @version 1.0
 */
struct ExecutorSendMsg {
    /** 用户认证框架的执行器索引。 */
    unsigned long executorIndex;
    /** 消息命令ID。 */
    int commandId;
    /** 执行器发送的消息。 */
    unsigned char[] msg;
};

/**
 * @brief 用户身份认证结果信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct AuthResultInfo {
    /** 用户身份认证结果。 */
    int result;
    /** 认证方式被冻结的时间。 */
    int lockoutDuration;
    /** 认证方式距离被冻结的可处理认证请求次数。 */
    int remainAttempts;
    /** 执行器发送的消息 @{ExecutorSendMsg}. */
    struct ExecutorSendMsg[] msgs;
    /** 用户身份认证令牌。 */
    unsigned char[] token;
    /** 保护文件加密密钥的密钥。 */
    unsigned char[] rootSecret;
};

/**
 * @brief 用户身份识别结果信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct IdentifyResultInfo {
    /** 用户身份识别结果。 */
    int result;
    /** 用户ID。 */
    int userId;
    /** 用户身份识别令牌。 */
    unsigned char[] token;
};

/**
 * @brief 注册认证凭据参数。
 *
 * @since 3.2
 * @version 1.0
 */
struct EnrollParam {
    /** 用户认证凭据类型@{AuthType}。 */
    enum AuthType authType;
    /** 既定用户认证凭据类型的执行器传感器提示，用于找到对应认证方式的传感器。 */
    unsigned int executorSensorHint;
};

/**
 * @brief 认证凭据信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct CredentialInfo {
    /** 认证凭据ID。 */
    unsigned long credentialId;
    /** 用户认证框架的执行器索引。 */
    unsigned long executorIndex;
    /** 认证凭据模版ID。 */
    unsigned long templateId;
    /** 用户认证凭据类型@{AuthType}。 */
    enum AuthType authType;
    /** 执行器匹配器。 */
    unsigned int executorMatcher;
    /** 既定用户认证凭据类型的执行器传感器提示，用于找到对应认证方式的传感器。 */
    unsigned int executorSensorHint;
};

/**
 * @brief 注册信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct EnrolledInfo {
    /** 注册ID，用户注册新的认证凭据时会更新注册ID。 */
    unsigned long enrolledId;
    /** 用户认证凭据类型@{AuthType}。 */
    enum AuthType authType;
};

/**
 * @brief 录入结果信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct EnrollResultInfo {
    /** 认证凭据ID */
    unsigned long credentialId;
    /** 旧凭据信息{@link CredentialInfo}。 */
    struct CredentialInfo oldInfo;
    /** 保护文件加密密钥的密钥。 */
    unsigned char[] rootSecret;
};
/** @} */