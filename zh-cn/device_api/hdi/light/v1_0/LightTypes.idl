/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Light
 * @{
 *
 * @brief 灯驱动对灯服务提供通用的接口能力。
 *
 * 灯模块为灯服务提供通用的接口去访问灯驱动。
 * 服务获取灯驱动对象或代理后，可以调用相关的APIs接口获取灯信息。
 * 打开或关闭灯，并根据灯类型ID设置灯闪烁模式
 * @since 3.1
 * @version 1.0
 */

/**
 * @file LightTypes.idl
 *
 * @brief 定义灯的数据结构，包括灯类型ID、灯的基本信息、灯的模式、灯的闪烁参数、灯的颜色模式和灯的效果参数。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @brief 灯模块接口的包路径。
 *
 * @since 3.1
 * @version 1.0
 */
package ohos.hdi.light.v1_0;

/**
 * @brief 枚举灯类型。
 *
 * @since 3.1
 * @version 1.0
 */
enum HdfLightId {
    HDF_LIGHT_ID_BATTERY = 1,           /**< 电源指示灯。 */
    HDF_LIGHT_ID_NOTIFICATIONS = 2,     /**< 通知灯。*/
    HDF_LIGHT_ID_ATTENTION = 3,         /**< 报警灯。 */
    HDF_LIGHT_ID_BUTT = 4,              /**< 无效ID。 */
};

/**
 * @brief 定义灯的基本信息。
 *
 * 参数包括灯类型ID、逻辑灯的名称、逻辑控制器中物理灯的数量和自定义扩展信息。
 *
 * @since 3.1
 * @version 1.0
 */
struct HdfLightInfo {
    String lightName;     /**< 逻辑灯的名称。 */
    int lightId;          /**< 灯类型ID。详见{@link HdfLightId}。 */
    int lightNumber;      /**< 逻辑控制器中物理灯的数量。 */
    int reserved;         /**< 自定义扩展信息。 */
};

/**
 * @brief 枚举灯的模式。
 *
 * @since 3.1
 * @version 1.0
 */
enum HdfLightFlashMode {
    HDF_LIGHT_FLASH_NONE = 0,       /**< 常亮模式。 */
    HDF_LIGHT_FLASH_BLINK = 1,      /**< 闪烁模式。 */
    HDF_LIGHT_FLASH_GRADIENT = 2,   /**< 渐变。 */
    HDF_LIGHT_FLASH_BUTT = 3,       /**< 无效模式。 */
};

/**
 * @brief 定义灯的闪烁参数。
 *
 * 这些参数包括闪烁模式以及闪烁期间指示灯的打开和关闭时间。
 *
 * @since 3.1
 * @version 1.0
 */
struct HdfLightFlashEffect {
   int flashMode;     /**< 闪烁模式。详见{@link LightFlashMode}。 */
   int onTime;        /**< 表示在一个闪烁周期内灯持续点亮的时间，单位毫秒。 */
   int offTime;       /**< 表示在一个闪烁周期内灯持续熄灭的时间，单位毫秒。 */
};

/**
 * @brief 定义灯的RGB模式。
 *
 * @since 3.2
 * @version 1.0
 */
struct RGBColor {
    int brightness;    /**< 亮度值，范围为0-255。 */
    int r;             /**< 红色值，范围为0-255。 */
    int g;             /**< 绿色值，范围为0-255。 */
    int b;             /**< 蓝色值，范围为0-255。 */
};

/**
 * @brief 定义灯的WRGB模式。
 *
 * @since 3.2
 * @version 1.0
 */
struct WRGBColor {
    int w;    /**< 白色值，范围为0-255。 */
    int r;    /**< 红色值，范围为0-255。 */
    int g;    /**< 绿色值，范围为0-255。 */
    int b;    /**< 蓝色值，范围为0-255。 */
};

/**
 * @brief 定义灯的颜色模式。
 *
 * 参数包括RGB模式和WRGB模式。
 *
 * @since 3.2
 * @version 1.0
 */
union ColorValue
{
    struct WRGBColor wrgbColor;    /**< WRGB模式, 详见{@link WRGBColor}。 */
    struct RGBColor rgbColor;      /**< RGB模式, 详见{@link RGBColor}。 */
};

/**
 * @brief 定义亮灯参数。
 *
 * 参数包括灯的模式设置。
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfLightColor {
    union ColorValue colorValue;    /**< 设置灯的模式, 详见{@link ColorValue}。 */
};

/**
 * @brief 定义灯的效果参数。
 *
 * 参数包括设置灯的亮度、闪烁模式。
 *
 * @since 3.1
 * @version 1.0
 */
struct HdfLightEffect {
    int lightBrightness;                 /**< 亮度值：Bits 24–31为扩展位，Bits 16–23为红色，Bits 8–15为绿色，Bits 0–7为蓝色。
                                              如果字节段等于0时，灯的亮度根据HCS配置的默认亮度进行设置。 */
    struct HdfLightFlashEffect flashEffect;   /**< 闪烁模式。详见{@link LightFlashEffect}。 */
};
/** @} */
