/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NATIVE_AVSCREEN_CAPTURE_H
#define NATIVE_AVSCREEN_CAPTURE_H

#include <stdint.h>
#include <stdio.h>
#include "native_avscreen_capture_errors.h"
#include "native_avscreen_capture_base.h"
#include "external_window.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup AVScreenCapture
 * @{
 * 
 * @brief 调用本模块下的接口，应用可以完成屏幕录制的功能。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @since 10
 */

/**
 * @file native_avscreen_capture.h
 * 
 * @brief 声明用于构造屏幕录制对象的API。
 *
 * @library libnative_avscreen_capture.so
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @since 10
 */

/**
 * @brief 创建OH_AVScreenCapture。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @return 返回一个capture 指向OH_AVScreenCapture实例的指针。
 * @since 10
 * @version 1.0
 */
struct OH_AVScreenCapture *OH_AVScreenCapture_Create(void);

/**
 * @brief 初始化OH_AVScreenCapture相关参数，包括下发的音频麦克风采样信息参数，音频内录相关参数(可选)，视频分辨率相关参数。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param config 录屏初始化相关参数。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_Init(struct OH_AVScreenCapture *capture,
    OH_AVScreenCaptureConfig config);

/**
 * @brief 启动录屏，调用此接口，可将录屏文件保存。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StartScreenCapture(struct OH_AVScreenCapture *capture);

/**
 * @brief 使用Surface模式录屏。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param window 指向OHNativeWindow实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StartScreenCaptureWithSurface(struct OH_AVScreenCapture *capture,
    OHNativeWindow* window);

/**
 * @brief 结束录屏，与OH_AVScreenCapture_StartScreenCapture配合使用。调用后针对调用该接口的应用会停止录屏或屏幕共享，释放麦克风。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StopScreenCapture(struct OH_AVScreenCapture *capture);

/**
 * @brief 启动录屏，调用此接口，可采集编码后的码流。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StartScreenRecording(struct OH_AVScreenCapture *capture);

/**
 * @brief 停止录屏，与OH_AVScreenCapture_StartScreenRecording配合使用。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_StopScreenRecording(struct OH_AVScreenCapture *capture);

/**
 * @brief 获取音频buffer，应用在调用时，需要对audiobuffer分配对应结构体大小的内存，否则会影响音频buffer的获取。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param audiobuffer 保存音频buffer的结构体，通过该结构体获取到音频buffer以及buffer的时间戳等信息。
 * @param type 音频buffer的类型，区分是麦克风录制的外部流还是系统内部播放音频的内录流，详情请参阅OH_AudioCaptureSourceType
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_OnBufferAvailable}
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_AcquireAudioBuffer(struct OH_AVScreenCapture *capture,
    OH_AudioBuffer **audiobuffer, OH_AudioCaptureSourceType type);

/**
 * @brief 获取视频buffer，应用在调用时，通过此接口来获取到视频的buffer以及时间戳等信息。
 * buffer使用完成后，调用OH_AVScreenCapture_ReleaseVideoBuffer接口进行视频buffer的释放。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param fence 用于同步的显示相关参数信息。
 * @param timestamp 视频帧的时间戳。
 * @param region 视频显示相关的坐标信息。
 * @return 执行成功返回OH_NativeBuffer对象，通过OH_NativeBuffer对象相关接口可以获取到视频buffer和分辨率等信息参数。
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_OnBufferAvailable}
 * @since 10
 * @version 1.0
 */
OH_NativeBuffer* OH_AVScreenCapture_AcquireVideoBuffer(struct OH_AVScreenCapture *capture,
    int32_t *fence, int64_t *timestamp, struct OH_Rect *region);

/**
 * @brief 根据音频类型释放buffer。当某一帧音频buffer使用完成后，调用此接口进行释放对应的音频buffer。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param type 音频buffer的类型，区分麦克风录制的外部流还是系统内部播放音频的内录流。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_OnBufferAvailable}
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ReleaseAudioBuffer(struct OH_AVScreenCapture *capture,
    OH_AudioCaptureSourceType type);

/**
 * @brief 根据视频类型释放buffer。当某一帧视频buffer使用完成后，调用此接口进行释放对应的视频buffer。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_OnBufferAvailable}
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ReleaseVideoBuffer(struct OH_AVScreenCapture *capture);

/**
 * @brief 设置监听接口，通过设置监听，可以监听到调用过程中的错误信息，以及是否有可用的视频buffer和音频buffer。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param callback OH_AVScreenCaptureCallback的结构体，保存相关回调函数指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @deprecated since 12
 * @useinstead {@link OH_AVScreenCapture_SetErrorCallback} {@link OH_AVScreenCapture_SetDataCallback}
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetCallback(struct OH_AVScreenCapture *capture,
    struct OH_AVScreenCaptureCallback callback);

/**
 * @brief 释放创建的OH_AVScreenCapture实例，对应OH_AVScreenCapture_Create。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_Release(struct OH_AVScreenCapture *capture);

/**
 * @brief 设置麦克风开关。当isMicrophone为true时，则打开麦克风，通过调用OH_AVScreenCapture_StartScreenCapture和
 * OH_AVScreenCapture_AcquireAudioBuffer可以正常获取到音频的麦克风原始PCM数据；isMicrophone为false时，获取到的音频数据为无声数据。 默认麦克风开关为开启。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param isMicrophone 麦克风开关参数。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 10
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetMicrophoneEnabled(struct OH_AVScreenCapture *capture,
    bool isMicrophone);
/**
 * @brief 设置状态回调，在开始录制前调用。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param callback OH_AVScreenCapture_OnStateChange实例。
 * @param userData 指向userData实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetStateCallback(struct OH_AVScreenCapture *capture,
    OH_AVScreenCapture_OnStateChange callback, void *userData);

/**
 * @brief 设置数据回调，在开始录制前调用。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param callback OH_AVScreenCapture_OnBufferAvailable实例。
 * @param userData 指向userData实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetDataCallback(struct OH_AVScreenCapture *capture,
    OH_AVScreenCapture_OnBufferAvailable callback, void *userData);

/**
 * @brief 设置错误回调，在开始录制前调用。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param callback OH_AVScreenCapture_OnError实例。
 * @param userData 指向userData实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetErrorCallback(struct OH_AVScreenCapture *capture,
    OH_AVScreenCapture_OnError callback, void *userData);

/**
 * @brief 设置屏幕旋转配置开关。当canvasRotation为true时，则打开屏幕旋转配置，通过调用OH_AVScreenCapture_StartScreenCapture后可以设置屏幕的旋转配置,
 * canvasRotation为true时，旋转配置生效，在横屏分辨率投屏录屏，屏幕竖起时录制画面为正向等比缩放的屏幕，两侧区域填充黑边。 默认屏幕旋转配置开关为关闭。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param canvasRotation 屏幕旋转配置参数。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK, 否则返回具体错误码，请参阅 OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_SetCanvasRotation(struct OH_AVScreenCapture *capture,
    bool canvasRotation);

/**
 * @brief 创建ContentFilter。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @return 执行成功返回OH_AVScreenCapture_ContentFilter实例，否则返回空指针。
 * @since 12
 * @version 1.0
 */
struct OH_AVScreenCapture_ContentFilter *OH_AVScreenCapture_CreateContentFilter(void);

/**
 * @brief 释放ContentFilter。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param filter 指向OH_AVScreenCapture_ContentFilter实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK，否则返回具体错误码，请参阅OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ReleaseContentFilter(struct OH_AVScreenCapture_ContentFilter *filter);

/**
 * @brief 增加过滤声音至ContentFilter。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param filter 指向OH_AVScreenCapture_ContentFilter实例的指针。
 * @param content OH_AVScreenCaptureFilterableAudioContent实例。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK，否则返回具体错误码，请参阅OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ContentFilter_AddAudioContent(
    struct OH_AVScreenCapture_ContentFilter *filter, OH_AVScreenCaptureFilterableAudioContent content);

/**
 * @brief ContentFilter排除过滤声音。
 * @syscap SystemCapability.Multimedia.Media.AVScreenCapture
 * @param capture 指向OH_AVScreenCapture实例的指针。
 * @param filter 指向OH_AVScreenCapture_ContentFilter实例的指针。
 * @return 执行成功返回AV_SCREEN_CAPTURE_ERR_OK，否则返回具体错误码，请参阅OH_AVSCREEN_CAPTURE_ErrCode。
 * @since 12
 * @version 1.0
 */
OH_AVSCREEN_CAPTURE_ErrCode OH_AVScreenCapture_ExcludeContent(struct OH_AVScreenCapture *capture,
    struct OH_AVScreenCapture_ContentFilter *filter);

#ifdef __cplusplus
}
#endif

/** @} */ 

#endif // NATIVE_AVSCREEN_CAPTURE_H