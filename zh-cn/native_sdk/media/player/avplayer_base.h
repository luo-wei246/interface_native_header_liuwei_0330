/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AVPlayer
 * @{
 *
 * @brief 为媒体源提供播放能力的API
 *
 * @Syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */

/**
 * @file avplayer_base.h
 *
 * @brief 定义AVPlayer的结构体和枚举
 *
 * @library libavplayer.so
 * @since 11
 * @version 1.0
 */

#ifndef MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_BASH_H
#define MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_BASH_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct OH_AVPlayer OH_AVPlayer;
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief 播放状态
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */
typedef enum AVPlayerState {
    /* 空闲 */
    AV_IDLE = 0,
    /* 初始化 */
    AV_INITIALIZED = 1,
    /* 准备 */
    AV_PREPARED = 2,
    /* 播放 */
    AV_PLAYING = 3,
    /* 暂停 */
    AV_PAUSED = 4,
    /* 停止 */
    AV_STOPPED = 5,
    /* 结束 */
    AV_COMPLETED = 6,
    /* 释放 */
    AV_RELEASED = 7,
    /* 错误 */
    AV_ERROR = 8,
} AVPlayerState;

/**
 * @brief 跳转模式
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */
typedef enum AVPlayerSeekMode {
    /* 同步到时间点之后的关键帧 */
    AV_SEEK_NEXT_SYNC = 0,
    /* 同步到时间点之前的关键帧 */
    AV_SEEK_PREVIOUS_SYNC,
} AVPlayerSeekMode;

/**
 * @brief 播放速度
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */
typedef enum AVPlaybackSpeed {
    /* 0.75倍速播放 */
    AV_SPEED_FORWARD_0_75_X,
    /* 正常播放 */
    AV_SPEED_FORWARD_1_00_X,
    /* 1.25倍速播放 */
    AV_SPEED_FORWARD_1_25_X,
    /* 1.75倍速播放 */
    AV_SPEED_FORWARD_1_75_X,
    /* 2.0倍速播放 */
    AV_SPEED_FORWARD_2_00_X,
} AVPlaybackSpeed;

/**
 * @brief OnInfo类型
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */
typedef enum AVPlayerOnInfoType {
    AV_INFO_TYPE_SEEKDONE = 0,
    AV_INFO_TYPE_SPEEDDONE = 1,
    AV_INFO_TYPE_BITRATEDONE = 2,
    AV_INFO_TYPE_EOS = 3,
    AV_INFO_TYPE_STATE_CHANGE = 4,
    AV_INFO_TYPE_POSITION_UPDATE = 5,
    AV_INFO_TYPE_MESSAGE = 6,
    AV_INFO_TYPE_VOLUME_CHANGE = 7,
    AV_INFO_TYPE_RESOLUTION_CHANGE = 8,
    AV_INFO_TYPE_BUFFERING_UPDATE = 9,
    AV_INFO_TYPE_BITRATE_COLLECT = 10,
    AV_INFO_TYPE_INTERRUPT_EVENT = 11,
    AV_INFO_TYPE_DURATION_UPDATE = 12,
    AV_INFO_TYPE_IS_LIVE_STREAM = 13,
    AV_INFO_TYPE_TRACKCHANGE = 14,
    AV_INFO_TYPE_TRACK_INFO_UPDATE = 15,
    AV_INFO_TYPE_SUBTITLE_UPDATE = 16,
    AV_INFO_TYPE_AUDIO_OUTPUT_DEVICE_CHANGE = 17
} AVPlayerOnInfoType;

/**
 * @brief 收到播放器消息时调用
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针
 * @param type 信息类型。具体请参见{@link AVPlayerOnInfoType}
 * @param extra 其他信息，如播放文件的开始时间位置
 * @since 11
 * @version 1.0
 */
typedef void (*OH_AVPlayerOnInfo)(OH_AVPlayer *player, AVPlayerOnInfoType type, int32_t extra);

/**
 * @brief 在api9以上的版本发生错误时调用
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param 指向OH_AVPlayer实例的指针
 * @param errorCode 错误码
 * @param errorMsg 错误消息
 * @since 11
 * @version 1.0
 */
typedef void (*OH_AVPlayerOnError)(OH_AVPlayer *player, int32_t errorCode, const char *errorMsg);

/**
 * @brief OH_AVPlayer中所有回调函数指针的集合。注册此的实例结构体到OH_AVPlayer实例中，并对回调上报的信息进行处理，保证AVPlayer的正常运行。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param onInfo 监控AVPlayer过程信息，参考{@link OH_AVPlayerOnInfo}
 * @param onError 监控AVPlayer操作错误，参考{@link OH_AVPlayerOnError}
 * @since 11
 * @version 1.0
 */
typedef struct AVPlayerCallback {
    OH_AVPlayerOnInfo onInfo;
    OH_AVPlayerOnError onError;
} AVPlayerCallback;


#ifdef __cplusplus
}
#endif
#endif // MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_BASH_H
