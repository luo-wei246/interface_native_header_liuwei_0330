/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Drm
 * @{
 *
 * @brief 提供数字版权保护能力的API。
 * @kit Drm.
 * @since 11
 * @version 1.0
 */

/**
 * @file native_mediakeysession.h
 * @brief 定义Drm MediaKeySession API。提供以下功能：
 * 生成媒体密钥请求、处理媒体密钥响应、事件侦听、获取内容保护级别、
 * 检查媒体密钥状态、删除媒体密钥等。
 * @library libnative_drm.z.so
 * @syscap SystemCapability.Multimedia.Drm.Core
 * @since 11
 * @version 1.0
 */

#ifndef OHOS_DRM_NATIVE_MEDIA_KEY_SESSION_H
#define OHOS_DRM_NATIVE_MEDIA_KEY_SESSION_H

#include <stdint.h>
#include <stdio.h>
#include "native_drm_err.h"
#include "native_drm_common.h"

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief  事件触发时将调用的回调。
 * @param eventType 事件类型。
 * @param info 从媒体密钥会话获取的事件信息。
 * @param infoLen 事件信息长度。
 * @param extra 从媒体密钥会话中获得的额外信息。
 * @return Drm_ErrCode 错误码。
 * @since 11
 * @version 1.0
 */
typedef  Drm_ErrCode (*MediaKeySession_EventCallback)(DRM_EventType eventType, uint8_t *info,
    int32_t infoLen, char *extra);

/**
 * @brief 密钥更改时将调用回调。
 * @param keysInfo 从媒体密钥系统获取的密钥信息。
 * @param newKeysAvailable 新密钥是否可用
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM_ERR_OK。
 * @since 11
 * @version 1.0
 */
typedef  Drm_ErrCode (*MediaKeySession_KeyChangeCallback)(DRM_KeysInfo *keysInfo, bool newKeysAvailable);

/**
 * @brief MediaKeySession_Callback结构体，用于监听密钥过期、密钥更改等事件。
 * @since 11
 * @version 1.0
 */
typedef struct MediaKeySession_Callback {
    /**
     * 正常事件回调，如密钥过期等。
     */
    MediaKeySession_EventCallback eventCallback;
    /**
     * 密钥更改事件的密钥更改回调。
     */
    MediaKeySession_KeyChangeCallback keyChangeCallback;
} MediaKeySession_Callback;

/**
 * @brief 生成媒体密钥请求。
 * @param mediaKeySession 媒体密钥会话实例。
 * @param info 媒体密钥请求信息。
 * @param mediaKeyRequest 媒体密钥请求。
 * @return Drm_ErrCode 错误码。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_GenerateMediaKeyRequest(MediaKeySession *mediaKeySession,
    DRM_MediaKeyRequestInfo *info, DRM_MediaKeyRequest *mediaKeyRequest);

/**
 * @brief 处理媒体密钥响应。
 * @param mediaKeySession 媒体密钥会话实例。
 * @param response 媒体密钥响应。
 * @param responseLen 媒体密钥响应长度。
 * @param offlineMediaKeyId 离线媒体密钥标识符。
 * @param offlineMediaKeyIdLen 缓冲区内和缓冲区外数据的离线媒体密钥标识符的长度。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM_ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_ProcessMediaKeyResponse(MediaKeySession *mediaKeySession,
    uint8_t *response, int32_t responseLen, uint8_t *offlineMediaKeyId, int32_t *offlineMediaKeyIdLen);

/**
 * @brief 检查媒体密钥状态。
 * @param mediaKeySession 媒体密钥会话实例。
 * @param mediaKeyStatus 媒体密钥状态。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM_ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_CheckMediaKeyStatus(MediaKeySession *mediaKeySessoin,
    DRM_MediaKeyStatus *mediaKeyStatus);

/**
 * @brief 清除当前会话的媒体密钥。
 * @param mediaKeySession 媒体密钥会话实例。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM_ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_ClearMediaKeys(MediaKeySession *mediaKeySessoin);

/**
 * @brief 生成离线媒体密钥释放请求。
 * @param mediaKeySession 媒体密钥会话实例。
 * @param offlineMediaKeyId 离线媒体密钥标识符。
 * @param releaseRequestLen 离线媒体密钥标识符长度。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM_ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_GenerateOfflineReleaseRequest(MediaKeySession *mediaKeySessoin,
    uint8_t *offlineMediaKeyId, int32_t offlineMediaKeyIdLen, uint8_t *releaseRequest,
    int32_t *releaseRequestLen);

/**
 * @brief 处理离线媒体密钥释放响应。
 * @param mediaKeySession 媒体密钥会话实例。
 * @param offlineMediaKeyId 离线媒体密钥标识符。
 * @param offlineMediaKeyIdLen 离线媒体密钥标识符长度。
 * @param releaseReponse 媒体密钥响应。
 * @param releaseReponseLen 媒体密钥响应长度。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM-ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_ProcessOfflineReleaseResponse(MediaKeySession *mediaKeySessoin,
    uint8_t *offlineMediaKeyId, int32_t offlineMediaKeyIdLen, uint8_t *releaseReponse,
    int32_t releaseReponseLen);

/**
 * @brief 按ID还原离线媒体密钥。
 * @param mediaKeySession 媒体密钥会话实例。
 * @param offlineMediaKeyId 离线媒体密钥标识符。
 * @param offlineMediaKeyIdLen 离线媒体密钥标识符长度。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM-ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_RestoreOfflineMediaKeys(MediaKeySession *mediaKeySessoin,
    uint8_t *offlineMediaKeyId, int32_t offlineMediaKeyIdLen);

/**
 * @brief 获取会话的内容保护级别。
 * @param mediaKeySession 媒体密钥会话实例。
 * @param contentProtectionLevel 内容保护级别。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM-ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_GetContentProtectionLevel(MediaKeySession *mediaKeySessoin,
    DRM_ContentProtectionLevel *contentProtectionLevel);

/**
 * @brief 加密内容是否需要安全解码。
 * @param mediaKeySession 媒体密钥会话实例。
 * @param mimeType 媒体类型。
 * @param status 是否需要安全解码。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM-ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_RequireSecureDecoderModule(MediaKeySession *mediaKeySessoin,
    const char *mimeType, bool *status);

/**
 * @brief 设置媒体密钥会话事件回调。
 * @param mediaKeySession 媒体密钥会话实例。
 * @param callback 要设置为媒体密钥会话的回调。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM-ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_SetMediaKeySessionCallback(MediaKeySession *mediaKeySessoin,
    MediaKeySession_Callback *callback);

/**
 * @brief 释放会话资源。
 * @param mediaKeySession 媒体密钥会话实例。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM-ERR_OK。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySession_Destroy(MediaKeySession *mediaKeySessoin);

#ifdef __cplusplus
}
#endif

#endif // OHOS_DRM_NATIVE_MEDIA_KEY_SYSTEM_H