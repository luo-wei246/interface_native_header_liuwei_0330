/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAudio
 * @{
 *
 * @brief 提供音频模块C接口定义。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 *
 * @since 10
 * @version 1.0
 */

/**
 * @file native_audiostreambuilder.h
 *
 * @brief 声明音频流构造器相关接口。
 *
 * 包含构造和销毁构造器，设置音频流属性，回调等相关接口。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 * @version 1.0
 */

#ifndef NATIVE_AUDIOSTREAMBUILDER_H
#define NATIVE_AUDIOSTREAMBUILDER_H

#include "native_audiostream_base.h"
#include "native_audiorenderer.h"
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建一个输入或者输出类型的音频流构造器。
 *
 * 当构造器不再使用时，需要调用OH_AudioStreamBuilder_Destroy()销毁它。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 该引用指向创建的构造器的结果。
 * @param type 构造器的流类型。{@link AUDIOSTREAM_TYPE_RERNDERER} or {@link AUDIOSTREAM_TYPE_CAPTURER}
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_Create(OH_AudioStreamBuilder** builder, OH_AudioStream_Type type);

/**
 * @brief 销毁一个音频流构造器。
 *
 * 当构造器不再使用时，需要调用该函数销毁它。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_Destroy(OH_AudioStreamBuilder* builder);

/**
 * @brief 设置音频流的采样率属性。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param channelCount 音频流采样率。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetSamplingRate(OH_AudioStreamBuilder* builder, int32_t rate);

/**
 * @brief 设置音频流的通道数属性。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param channelCount 音频流通道数。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetChannelCount(OH_AudioStreamBuilder* builder, int32_t channelCount);

/**
 * @brief 设置音频流的采样格式属性。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param format 音频流采样格式。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetSampleFormat(OH_AudioStreamBuilder* builder,
    OH_AudioStream_SampleFormat format);

/**
 * @brief 设置音频流的编码类型属性。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param encodingType 音频流编码类型, {@link AUDIOSTREAM_ENCODING_PCM}
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetEncodingType(OH_AudioStreamBuilder* builder,
    OH_AudioStream_EncodingType encodingType);

/**
 * @brief 设置音频流的时延模式。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param latencyMode 音频流时延模式。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetLatencyMode(OH_AudioStreamBuilder* builder,
    OH_AudioStream_LatencyMode latencyMode);

/**
 * @brief 设置输出音频流的工作场景。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param usage 输出音频流属性，使用的工作场景。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetRendererInfo(OH_AudioStreamBuilder* builder,
    OH_AudioStream_Usage usage);

/**
 * @brief 设置输入音频流的工作场景。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param sourceType 输入音频流属性，使用的工作场景。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetCapturerInfo(OH_AudioStreamBuilder* builder,
    OH_AudioStream_SourceType sourceType);

/**
 * @brief 设置输出音频流的回调。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param callbacks 将被用来处理输出音频流相关事件的回调函数。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetRendererCallback(OH_AudioStreamBuilder* builder,
    OH_AudioRenderer_Callbacks callbacks, void* userData);

/**
 * @brief 设置输出音频流设备变更的回调。
 *
 * @since 11
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param callbacks 将被用来处理输出流设备变更相关事件的回调函数。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetRendererOutputDeviceChangeCallback(OH_AudioStreamBuilder* builder,
    OH_AudioRenderer_OutputDeviceChangeCallback callback, void* userData);

/**
 * @brief 设置输入音频流的回调。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param callbacks 将被用来处理输入音频流相关事件的回调函数。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetCapturerCallback(OH_AudioStreamBuilder* builder,
    OH_AudioCapturer_Callbacks callbacks, void* userData);

/**
 * @brief 创建输出音频流实例。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param audioRenderer 指向输出音频流实例的指针，将被用来接收函数创建的结果。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_GenerateRenderer(OH_AudioStreamBuilder* builder,
    OH_AudioRenderer** audioRenderer);

/**
 * @brief 创建输入音频流实例。
 *
 * @since 10
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param audioCapturer 指向输入音频流实例的指针，将被用来接收函数创建的结果。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_GenerateCapturer(OH_AudioStreamBuilder* builder,
    OH_AudioCapturer** audioCapturer);

/**
 * @brief 设置每次回调的帧长，帧长至少为音频硬件一次处理的数据大小，并且小于内部缓冲容量的一半。
 *
 * @since 11
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @param builder 指向OH_AudioStreamBuilder_Create()创建的构造器实例。
 * @param frameSize 要设置音频数据的帧长。
 * @return {@link AUDIOSTREAM_SUCCESS} 或者一个不期望发生的错误。
 */
OH_AudioStream_Result OH_AudioStreamBuilder_SetFrameSizeInCallback(OH_AudioStreamBuilder* builder,
    int32_t frameSize);
#ifdef __cplusplus
}
#endif

#endif // NATIVE_AUDIOSTREAMBUILDER_H
/** @} */