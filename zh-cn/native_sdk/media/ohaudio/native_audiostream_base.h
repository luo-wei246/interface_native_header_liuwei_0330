/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAudio
 * @{
 *
 * @brief 提供音频模块C接口定义。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 *
 * @since 10
 * @version 1.0
 */

/**
 * @file native_audiostream_base.h
 *
 * @brief 声明OHAudio基础的数据结构。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 * @version 1.0
 */

#ifndef NATIVE_AUDIOSTREAM_BASE_H
#define NATIVE_AUDIOSTREAM_BASE_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 音频错误码。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 操作成功
     */
    AUDIOSTREAM_SUCCESS = 0,

    /**
     * 入参错误。
     */
    AUDIOSTREAM_ERROR_INVALID_PARAM = 1,

    /**
     * 非法状态。
     */
    AUDIOSTREAM_ERROR_ILLEGAL_STATE = 2,

    /**
     * 系统通用错误。
     */
    AUDIOSTREAM_ERROR_SYSTEM = 3
} OH_AudioStream_Result;

/**
 * @brief 音频流类型。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 该类型代表音频流是输出流。
     */
    AUDIOSTREAM_TYPE_RERNDERER = 1,

    /**
     * 该类型代表音频流是输入流。
     */
    AUDIOSTREAM_TYPE_CAPTURER = 2
} OH_AudioStream_Type;

/**
 * @brief 定义音频流采样格式。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * Unsigned 8位。
     */
    AUDIOSTREAM_SAMPLE_U8 = 0,
    /**
     * Short 16位小端。
     */
    AUDIOSTREAM_SAMPLE_S16LE = 1,
    /**
     * Short 24位小端。
     */
    AUDIOSTREAM_SAMPLE_S24LE = 2,
    /**
     * Short 32位小端。
     */
    AUDIOSTREAM_SAMPLE_S32LE = 3,
} OH_AudioStream_SampleFormat;

/**
 * @brief 定义音频流编码类型。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * PCM编码。
     */
    AUDIOSTREAM_ENCODING_TYPE_RAW = 0,
} OH_AudioStream_EncodingType;

/**
 * @brief 定义音频流使用场景。
 *
 * 通常用来描述音频输出流的使用场景。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 未定义。
     */
    AUDIOSTREAM_USAGE_UNKNOWN = 0,
    /**
     * 音乐。
     */
    AUDIOSTREAM_USAGE_MUSIC = 1,
    /**
     * 通话。
     */
    AUDIOSTREAM_USAGE_COMMUNICATION = 2,
    /**
     * 语音助手。
     */
    AUDIOSTREAM_USAGE_VOICE_ASSISTANT = 3,
    /**
     * 闹钟。
     */
    AUDIOSTREAM_USAGE_ALARM = 4,
    /**
     * 语音消息。
     */
    AUDIOSTREAM_USAGE_VOICE_MESSAGE = 5,
    /**
     * 铃声。
     */
    AUDIOSTREAM_USAGE_RINGTONE = 6,
    /**
     * 通知。
     */
    AUDIOSTREAM_USAGE_NOTIFICATION = 7,
    /**
     * 无障碍。
     */
    AUDIOSTREAM_USAGE_ACCESSIBILITY = 8,
    /**
     * 视频。
     */
    AUDIOSTREAM_USAGE_MOVIE = 10,
    /**
     * 游戏。
     */
    AUDIOSTREAM_USAGE_GAME = 11,
    /**
     * 有声读物。
     */
    AUDIOSTREAM_USAGE_AUDIOBOOK = 12,
    /**
     * 导航。
     */
    AUDIOSTREAM_USAGE_NAVIGATION = 13
} OH_AudioStream_Usage;

/**
 * @brief 定义音频时延模式。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 该模式代表一个普通时延的音频流。
     */
    AUDIOSTREAM_LATENCY_MODE_NORMAL = 0,
    /**
     * 该模式代表一个低时延的音频流。
     */
    AUDIOSTREAM_LATENCY_MODE_FAST = 1
} OH_AudioStream_LatencyMode;

/**
 * @brief 定义音频流的状态。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 不合法的状态。
     */
    AUDIOSTREAM_STATE_INVALID = -1,
    /**
     * 新创建时的状态。
     */
    AUDIOSTREAM_STATE_NEW = 0,
    /**
     * 准备状态。
     */
    AUDIOSTREAM_STATE_PREPARED = 1,
    /**
     * 工作状态。
     */
    AUDIOSTREAM_STATE_RUNNING = 2,
    /**
     * 停止状态。
     */
    AUDIOSTREAM_STATE_STOPPED = 3,
    /**
     * 释放状态。
     */
    AUDIOSTREAM_STATE_RELEASED = 4,
    /**
     * 暂停状态。
     */
    AUDIOSTREAM_STATE_PAUSED = 5,
} OH_AudioStream_State;

/**
 * @brief 定义音频流使用场景。
 *
 * 通常用来描述音频输入流的使用场景。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 不合法状态。
     */
    AUDIOSTREAM_SOURCE_TYPE_INVALID = -1,
    /**
     * 录音。
     */
    AUDIOSTREAM_SOURCE_TYPE_MIC = 0,
    /**
     * 语音识别。
     */
    AUDIOSTREAM_SOURCE_TYPE_VOICE_RECOGNITION = 1,
    /**
     * 播放录音。
     */
    AUDIOSTREAM_SOURCE_TYPE_PLAYBACK_CAPTURE = 2,
    /**
     * 通话。
     */
    AUDIOSTREAM_SOURCE_TYPE_VOICE_COMMUNICATION = 7
} OH_AudioStream_SourceType;

/**
 * @brief 定义音频事件。
 *
 * 通常用来描述音频事件。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 音频的路由已更改。
     */
    AUDIOSTREAM_EVENT_ROUTING_CHANGED = 0
} OH_AudioStream_Event;

/**
 * @brief 定义音频中断类型。
 *
 * 通常用来描述音频中断事件。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 强制类型，系统更改音频状态。
     */
    AUDIOSTREAM_INTERRUPT_FORCE = 0,
    /**
     * 共享类型，应用程序更改音频状态。
     */
    AUDIOSTREAM_INTERRUPT_SHAR = 1
} OH_AudioInterrupt_ForceType;

/**
 * @brief 定义音频中断类型。
 *
 * 通常用来描述音频中断事件。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 不提示。
     */
    AUDIOSTREAM_INTERRUPT_HINT_NONE = 0,
    /**
     * 恢复流提示。
     */
    AUDIOSTREAM_INTERRUPT_HINT_RESUME = 1,
    /**
     * 暂停流提示。
     */
    AUDIOSTREAM_INTERRUPT_HINT_PAUSE = 2,
    /**
     * 停止流提示。
     */
    AUDIOSTREAM_INTERRUPT_HINT_STOP = 3,
    /**
     * 短暂降低音量。
     */
    AUDIOSTREAM_INTERRUPT_HINT_DUCK = 4,
    /**
     * 恢复音量。
     */
    AUDIOSTREAM_INTERRUPT_HINT_UNDUCK = 5
} OH_AudioInterrupt_Hint;

/**
 * @brief 声明音频流的构造器。
 *
 * 构造器实例通常被用来设置音频流属性和创建音频流。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioStreamBuilderStruct OH_AudioStreamBuilder;

/**
 * @brief 声明输出音频流。
 *
 * 输出音频流的实例被用来播放音频数据。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioRendererStruct OH_AudioRenderer;

/**
 * @brief 声明输入音频流。
 *
 * 输入音频流的实例被用来获取音频数据。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioCapturerStruct OH_AudioCapturer;

/**
 * @brief 声明输出音频流的回调函数指针。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioRenderer_Callbacks_Struct {
    /**
     * 这个函数指针将指向用于写入音频数据的回调函数。
     */
    int32_t (*OH_AudioRenderer_OnWriteData)(
            OH_AudioRenderer* renderer,
            void* userData,
            void* buffer,
            int32_t lenth);

    /**
     * 这个函数指针将指向用于处理音频播放流事件的回调函数。
     */
    int32_t (*OH_AudioRenderer_OnStreamEvent)(
            OH_AudioRenderer* renderer,
            void* userData,
            OH_AudioStream_Event event);

    /**
     * 这个函数指针将指向用于处理音频播放中断事件的回调函数。
     */
    int32_t (*OH_AudioRenderer_OnInterruptEvent)(
            OH_AudioRenderer* renderer,
            void* userData,
            OH_AudioInterrupt_ForceType type,
            OH_AudioInterrupt_Hint hint);

    /**
     * 这个函数指针将指向用于处理音频播放错误结果的回调函数。
     */
    int32_t (*OH_AudioRenderer_OnError)(
            OH_AudioRenderer* renderer,
            void* userData,
            OH_AudioStream_Result error);
} OH_AudioRenderer_Callbacks;

/**
 * @brief 声明输入音频流的回调函数指针。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioCapturer_Callbacks_Struct {
    /**
     * 这个函数指针将指向用于读取音频数据的回调函数。
     */
    int32_t (*OH_AudioCapturer_OnReadData)(
            OH_AudioCapturer* capturer,
            void* userData,
            void* buffer,
            int32_t lenth);

    /**
     * 这个函数指针将指向用于处理音频录制流事件的回调函数。
     */
    int32_t (*OH_AudioCapturer_OnStreamEvent)(
            OH_AudioCapturer* capturer,
            void* userData,
            OH_AudioStream_Event event);

    /**
     * 这个函数指针将指向用于处理音频录制中断事件的回调函数。
     */
    int32_t (*OH_AudioCapturer_OnInterruptEvent)(
            OH_AudioCapturer* capturer,
            void* userData,
            OH_AudioInterrupt_ForceType type,
            OH_AudioInterrupt_Hint hint);

    /**
     * 这个函数指针将指向用于处理音频录制错误结果的回调函数。
     */
    int32_t (*OH_AudioCapturer_OnError)(
            OH_AudioCapturer* capturer,
            void* userData,
            OH_AudioStream_Result error);
} OH_AudioCapturer_Callbacks;

/**
 * @brief 流设备变更原因。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 11
 */
typedef enum {
    /**
     * 未知原因。
     */
    REASON_UNKNOWN = 0,
    /**
     * 新设备可用。
     */
    REASON_NEW_DEVICE_AVAILABLE = 1,
    /**
     * 旧设备不可用。当报告此原因时，应用程序应考虑暂停音频播放。
     */
    REASON_OLD_DEVICE_UNAVAILABLE = 2,
    /**
     * 用户或系统强制选择切换。
     */
    REASON_OVERRODE = 3
} OH_AudioStream_DeviceChangeReason;

/**
 * @brief 输出音频流设备变更的回调函数。
 *
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @param reason 流设备变更原因。
 * @since 11
 */
typedef void (*OH_AudioRenderer_OutputDeviceChangeCallback)(OH_AudioRenderer* renderer, void* userData,
    OH_AudioStream_DeviceChangeReason reason);
#ifdef __cplusplus
}
#endif

#endif // NATIVE_AUDIOSTREAM_BASE_H
/** @} */