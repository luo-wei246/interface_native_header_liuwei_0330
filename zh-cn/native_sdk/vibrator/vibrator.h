/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup 马达
 * @{
 *
 * @brief 为马达服务提供统一的API以访问马达驱动程序。
 * @since 11
 */

/**
 * @file vibrator.h
 *
 * @brief 为您提供标准的开放api，用于控制马达振动的启停。
 * @library libohvibrator.z.so
 * @syscap SystemCapability.Sensors.MiscDevice
 * @since 11
 */

#ifndef VIBRATOR_H
#define VIBRATOR_H

#include "vibrator_type.h"

#ifdef __cplusplus
extern "C" {
#endif

namespace OHOS {
namespace Sensors {
/**
 * @brief 控制马达在指定时间内持续振动。
 *
 * @param duration - 振动时长，单位：毫秒。
 * @param attribute - 振动属性，请参考｛@Link VibrateAttribute｝。
 * @return 如果操作成功，则返回0；否则返回非零值。请参阅 {@link Vibrator_ErrorCode}。
 * @permission ohos.permission.VIBRATE
 *
 * @since 11
 */
int32_t OH_Vibrator_PlayVibration(int32_t duration, Vibrator_Attribute attribute);

/**
 * @brief 播放自定义振动序列。
 *
 * @param fileDescription - 自定义振动效果文件描述符，请参阅 {@link Vibrator_FileDescription}。
 * @param vibrateAttribute - 振动属性，请参阅 {@link Vibrator_Attribute}。
 * @return 如果操作成功，则返回0；否则返回非零值。请参阅 {@link Vibrator_ErrorCode}。
 * @permission ohos.permission.VIBRATE
 *
 * @since 11
 */
int32_t OH_Vibrator_PlayVibrationCustom(Vibrator_FileDescription fileDescription,
    Vibrator_Attribute vibrateAttribute);

/**
 * @brief 停止马达振动。
 *
 * @return 如果操作成功，则返回0；否则返回非零值。请参阅 {@link Vibrator_ErrorCode}。
 * @permission ohos.permission.VIBRATE
 *
 * @since 11
 */
int32_t OH_Vibrator_Cancel();
} // namespace Sensors
} // namespace OHOS
#ifdef __cplusplus
};
#endif
/** @} */
#endif // endif VIBRATOR_H