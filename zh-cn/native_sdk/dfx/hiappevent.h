/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIVIEWDFX_HIAPPEVENT_H
#define HIVIEWDFX_HIAPPEVENT_H
/**
 * @addtogroup HiAppEvent
 * @{
 *
 * @brief HiAppEvent模块提供应用事件打点功能。
 *
 * 为应用程序提供事件打点功能，记录运行过程中上报的故障事件、统计事件、安全事件和用户行为事件。基于事件信息，您可以分析应用的运行状态。
 *
 * @syscap SystemCapability.HiviewDFX.HiAppEvent
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file hiappevent.h
 *
 * @brief HiAppEvent模块的应用事件打点函数定义。
 *
 * 在执行应用事件打点之前，您必须先构造一个参数列表对象来存储输入的事件参数，并指定事件领域、事件名称和事件类型。
 *
 * <p>事件领域：用于标识事件打点的领域的字符串。
 * <p>事件名称：用于标识事件打点的名称的字符串。
 * <p>事件类型：故障、统计、安全、行为。
 * <p>参数列表：用于存储事件参数的链表，每个参数由参数名和参数值组成。
 *
 * 示例代码:
 * 00 引入头文件:
 * <pre>
 *     #include "hiappevent/hiappevent.h"
 * </pre>
 * 01 创建一个参数列表指针：
 * <pre>
 *     ParamList list = OH_HiAppEvent_CreateParamList();
 * </pre>
 * 02 添加参数到参数列表中：
 * <pre>
 *     bool boolean = true;
 *     OH_HiAppEvent_AddBoolParam(list, "bool_key", boolean);
 *     int32_t nums[] = {1, 2, 3};
 *     OH_HiAppEvent_AddInt32ArrayParam(list, "int32_arr_key", nums, sizeof(nums) / sizeof(nums[0]));
 * </pre>
 * 03 执行事件打点：
 * <pre>
 *     int res = OH_HiAppEvent_Write("test_domain", "test_event", BEHAVIOR, list);
 * </pre>
 * 04 销毁参数列表指针，释放其分配内存：
 * <pre>
 *     OH_HiAppEvent_DestroyParamList(list);
 * </pre>
 *
 * @since 8
 * @version 1.0
 */

#include <stdbool.h>
#include <stdint.h>

#include "hiappevent_cfg.h"
#include "hiappevent_event.h"
#include "hiappevent_param.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 事件类型。
 *
 * 建议您根据不同的使用场景选择不同的事件类型。
 *
 * @since 8
 * @version 1.0
 */
enum EventType {
    /* 故障事件类型 */
    FAULT = 1,

    /* 统计事件类型 */
    STATISTIC = 2,

    /* 安全事件类型 */
    SECURITY = 3,

    /* 行为事件类型 */
    BEHAVIOR = 4
};

/**
 * @brief 单个事件信息，包含事件领域，事件名称，事件类型和json格式字符串表示的事件中携带的自定义参数列表。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @since 12
 * @version 1.0
 */
typedef struct HiAppEvent_AppEventInfo {
    /** 事件领域。 */
    const char* domain;
    /** 事件名称。 */
    const char* name;
    /** 事件类型。 */
    enum EventType type;
    /** Json格式字符串类型的事件参数列表。 */
    const char* params;
} HiAppEvent_AppEventInfo;

/**
 * @brief 具有相同事件名称的事件组。
 *
 * @syscap SystemCapability.HiviewDFX.HiAppEvent
 * @since 12
 * @version 1.0
 */
typedef struct HiAppEvent_AppEventGroup {
    /** 事件数组中相同的事件名称。 */
    const char* name;
    /** 具有相同事件名称的事件数组。 */
    const struct HiAppEvent_AppEventInfo* appEventInfos;
    /** 具有相同事件名称的事件数组的长度。 */
    uint32_t infoLen;
} HiAppEvent_AppEventGroup;

/**
 * @brief 事件参数列表节点。
 *
 * @since 8
 * @version 1.0
 */
typedef struct ParamListNode* ParamList;

/**
 * @brief 用于接收app事件的监听器。
 *
 * @syscap SystemCapability.HiviewDFX.HiAppEvent
 * @since 12
 * @version 1.0
 */
typedef struct HiAppEvent_Watcher HiAppEvent_Watcher;

/**
 * @brief 监听器接收到事件后，将触发该回调，将事件内容传递给调用方。
 *
 * 注意：回调中的指针所指对象的生命周期仅限于该回调函数内，请勿在该回调函数外直接使用该指针，若需缓存该信息，请对指针指向的内容进行深拷贝。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param domain 接收到的app事件的领域。
 * @param appEventGroups 按照不同事件名称分组的事件组数组。
 * @param groupLen 事件组数组的长度。
 * @since 12
 * @version 1.0
 */
typedef void (*OH_HiAppEvent_OnReceive)(
    const char* domain, const struct HiAppEvent_AppEventGroup* appEventGroups, uint32_t groupLen);

/**
 * @brief 监听器收到事件后，若监听器中未设置OH_HiAppEvent_OnReceive回调，将保存该事件。当保存的事件满足通过
 * OH_HiAppEvent_SetTriggerCondition设定的条件后，将触发该回调。回调结束后，当新保存的事件消息再次满足设定的条件后，将再次进行回调。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param row 监听器新接收到的事件消息的数量。
 * @param size 监听器新接收的事件消息的大小总和(单个事件大小计算方式为，将消息转换为json字符串后，字符串的长度)。
 * @since 12
 * @version 1.0
 */
typedef void (*OH_HiAppEvent_OnTrigger)(int row, int size);

/**
 * @brief 使用OH_HiAppEvent_TakeWatcherData获取监听器接收到的事件时，监听器接收到的事件将通过该回调函数传递给调用者。
 *
 * 注意：回调中的指针所指对象的生命周期仅限于该回调函数内，请勿在该回调函数外直接使用该指针，若需缓存该信息，请对指针指向的内容进行深拷贝。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param events json字符串格式的事件数组。
 * @param eventLen 事件数组大小。
 * @since 12
 * @version 1.0
 */
typedef void (*OH_HiAppEvent_OnTake)(const char* const *events, uint32_t eventLen);

/**
 * @brief 创建一个指向参数列表对象的指针。
 *
 * @return 指向参数列表对象的指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_CreateParamList(void);

/**
 * @brief 销毁一个指向参数列表对象的指针，释放其分配内存。
 *
 * @param list 参数列表对象指针。
 * @since 8
 * @version 1.0
 */
void OH_HiAppEvent_DestroyParamList(ParamList list);

/**
 * @brief 添加一个布尔参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param boolean 需要添加的布尔参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddBoolParam(ParamList list, const char* name, bool boolean);

/**
 * @brief 添加一个布尔数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param booleans 需要添加的布尔数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddBoolArrayParam(ParamList list, const char* name, const bool* booleans, int arrSize);

/**
 * @brief 添加一个int8_t参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的int8_t参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt8Param(ParamList list, const char* name, int8_t num);

/**
 * @brief 添加一个int8_t数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的int8_t数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt8ArrayParam(ParamList list, const char* name, const int8_t* nums, int arrSize);

/**
 * @brief 添加一个int16_t参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的int16_t参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt16Param(ParamList list, const char* name, int16_t num);

/**
 * @brief 添加一个int16_t数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的int16_t数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt16ArrayParam(ParamList list, const char* name, const int16_t* nums, int arrSize);

/**
 * @brief 添加一个int32_t参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的int32_t参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt32Param(ParamList list, const char* name, int32_t num);

/**
 * @brief 添加一个int32_t数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的int32_t数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt32ArrayParam(ParamList list, const char* name, const int32_t* nums, int arrSize);

/**
 * @brief 添加一个int64_t参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的int64_t参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt64Param(ParamList list, const char* name, int64_t num);

/**
 * @brief 添加一个int64_t数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的int64_t数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt64ArrayParam(ParamList list, const char* name, const int64_t* nums, int arrSize);

/**
 * @brief 添加一个float参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的float参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddFloatParam(ParamList list, const char* name, float num);

/**
 * @brief 添加一个float数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的float数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddFloatArrayParam(ParamList list, const char* name, const float* nums, int arrSize);

/**
 * @brief 添加一个double参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的double参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddDoubleParam(ParamList list, const char* name, double num);

/**
 * @brief 添加一个double数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的double数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddDoubleArrayParam(ParamList list, const char* name, const double* nums, int arrSize);

/**
 * @brief 添加一个字符串参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param str 需要添加的字符串参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddStringParam(ParamList list, const char* name, const char* str);

/**
 * @brief 添加一个字符串数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param strs 需要添加的字符串数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddStringArrayParam(ParamList list, const char* name, const char * const *strs, int arrSize);

/**
 * @brief 实现对参数为列表类型的应用事件打点。
 *
 * 在应用事件打点前，该接口会先对该事件的参数进行校验。如果校验成功，则接口会将事件写入事件文件。
 *
 * @param domain 事件领域。您可以根据需要自定义事件领域。
 * @param name 事件名称。您可以根据需要自定义事件名称。
 * @param type 事件类型，在{@link EventType}中定义。
 * @param list 事件参数列表，每个参数由参数名和参数值组成。
 * @return 如果事件参数校验成功，则返回{@code 0}，将事件写入事件文件；如果事件中存在无效参数，则返回正值，丢弃
 * 无效参数后将事件写入事件文件；如果事件参数校验失败，则返回负值，并且事件将不会写入事件文件。
 * @since 8
 * @version 1.0
 */
int OH_HiAppEvent_Write(const char* domain, const char* name, enum EventType type, const ParamList list);

/**
 * @brief 实现应用事件打点的配置功能。
 *
 * 应用事件打点配置接口，用于配置事件打点开关、事件文件目录存储配额大小等功能。
 *
 * @param name 配置项名称。
 * @param value 配置项值。
 * @return 配置结果。
 * @since 8
 * @version 1.0
 */
bool OH_HiAppEvent_Configure(const char* name, const char* value);

/**
 * @brief 创建一个用来监听app事件的监听器。
 *
 * 注意：创建的监听器不再使用后必须通过调用OH_HiAppEvent_DestroyWatcher接口进行销毁。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param name 监听器名称。
 * @return 返回指向的新建监听器的指针。
 * @since 12
 * @version 1.0
 */
HiAppEvent_Watcher* OH_HiAppEvent_CreateWatcher(const char* name);

/**
 * @brief 销毁已创建的监听器。
 *
 * 注意：已创建的监听器不再使用后，需要将其销毁，释放内存，防止内存泄漏，销毁后需将对应指针置空。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param watcher 指向监听器的指针（即OH_HiAppEvent_CreateWatcher接口返回的指针）。
 * @since 12
 * @version 1.0
 */
void OH_HiAppEvent_DestroyWatcher(HiAppEvent_Watcher* watcher);

/**
 * @brief 用来设置监听器OH_HiAppEvent_OnTrigger回调的触发条件，分别可以从监视器新接收事件数量，新接收事件大小，onTrigger触发超时时间
 * 设置触发条件，调用方应至少保证从一个方面设置触发条件。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param watcher 指向监听器的指针（即OH_HiAppEvent_CreateWatcher接口返回的指针）。
 * @param row 当输入值大于0，且新接收事件的数量大于等于该值时，将调用设置的onTrigger回调函数吗，当输入值小于等于0时，不再以接收数量多少为维度
 * 来触发onTrigger回调。
 * @param size 当输入值大于0，且新接收事件的大小(单个事件大小计算方式为，将事件转换为json字符串后，字符串的长度)大于等于该值时，将调用设置的
 * onTrigger回调函数，当输入值小于等于0时，不再以新接收事件大小为维度触发onTrigger回调。
 * @param timeOut 单位秒，当输入值大于0，每经过timeout秒，将检查监视器是否存在新接收到的事件，如果存在将触发onTrigger回调。触发onTrigger
 * 后，经过timeOut秒后将再次检查是否存在新接收到的事件。当输入值小于等于0，不以超时时间为维度触发onTrigger回调。
 * @return Returns {@code 0} 设置成功时返回0，设置失败时返回负值。
 * @since 12
 * @version 1.0
 */
int OH_HiAppEvent_SetTriggerCondition(HiAppEvent_Watcher* watcher, int row, int size, int timeOut);

/**
 * @brief 用来设置监听器需要监听的事件的类型。
 *
 * 该函数可以重复调用，可添加多个过滤规则，而非替换，监听器将收到满足任一过滤规则的事件的通知。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param watcher 指向监听器的指针（即OH_HiAppEvent_CreateWatcher接口返回的指针）。
 * @param domain 需要监听事件的领域。
 * @param eventTypes 需要监听事件的事件类型。
 * @param names 需要监听的事件名称数组。
 * @param namesLen 监听的事件名称的数组长度。
 * @return Returns {@code 0} 设置成功时返回0，设置失败时返回负值。
 * @since 12
 * @version 1.0
 */
int OH_HiAppEvent_SetAppEventFilter(HiAppEvent_Watcher* watcher, const char* domain, uint8_t eventTypes,
    const char* const *names, int namesLen);

/**
 * @brief 用来设置监听器onTrigger回调的接口。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param watcher 指向监听器的指针（即OH_HiAppEvent_CreateWatcher接口返回的指针）。
 * @param onTrigger 需要设置的回调。
 * @return Returns {@code 0} 接口调用成功返回0，异常时返回负值。
 * @since 12
 * @version 1.0
 */
int OH_HiAppEvent_SetWatcherOnTrigger(HiAppEvent_Watcher* watcher, OH_HiAppEvent_OnTrigger onTrigger);

/**
 * @brief 用来设置监听器onReceive回调函数的接口。当监听器监听到相应事件后，onReceive回调函数将被调用。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param watcher 指向监听器的指针（即OH_HiAppEvent_CreateWatcher接口返回的指针）。
 * @param onReceive 回调函数的函数指针。
 * @return Returns {@code 0} 接口调用成功返回0，异常时返回负值。
 * @since 12
 * @version 1.0
 */
int OH_HiAppEvent_SetWatcherOnReceive(HiAppEvent_Watcher* watcher, OH_HiAppEvent_OnReceive onReceive);

/**
 * @brief 用来获取监听器收到后保存的事件。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param watcher 指向监听器的指针（即OH_HiAppEvent_CreateWatcher接口返回的指针）。
 * @param eventNum 需要获取的事件数量。
 * @param onTake 回调函数指针，事件通过调用该函数返回事件信息。
 * @return Returns {@code 0} 接口调用成功返回0，异常时返回负值。
 * @since 12
 * @version 1.0
 */
int OH_HiAppEvent_TakeWatcherData(HiAppEvent_Watcher* watcher, uint32_t eventNum, OH_HiAppEvent_OnTake onTake);

/**
 * @brief 添加监听器的接口，监听器开始监听系统消息。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param watcher 指向监听器的指针（即OH_HiAppEvent_CreateWatcher接口返回的指针）。
 * @return Returns {@code 0} 接口调用成功返回0，异常时返回负值。
 * @since 12
 * @version 1.0
 */
int OH_HiAppEvent_AddWatcher(HiAppEvent_Watcher* watcher);

/**
 * @brief 移除监听器的接口，监听器停止监听系统消息。
 *
 * 注意：该接口仅仅使监听器停止监听系统消息，并未销毁该监听器，该监听器依然常驻内存，直至调用OH_HiAppEvent_DestroyWatcher接口，内存才会释放。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @param watcher 指向监听器的指针（即OH_HiAppEvent_CreateWatcher接口返回的指针）。
 * @return Returns {@code 0} 接口调用成功返回0，异常时返回负值。
 * @since 12
 * @version 1.0
 */
int OH_HiAppEvent_RemoveWatcher(HiAppEvent_Watcher* watcher);

/**
 * @brief 清除所有监视器保存的所有事件。
 *
 * @SystemCapability.HiviewDFX.HiAppEvent
 * @since 12
 * @version 1.0
 */
void OH_HiAppEvent_ClearData(void);
#ifdef __cplusplus
}
#endif
/** @} */
#endif // HIVIEWDFX_HIAPPEVENT_H