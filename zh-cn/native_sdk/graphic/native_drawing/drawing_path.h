/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PATH_H
#define C_INCLUDE_DRAWING_PATH_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块不提供像素单位，和应用上下文环境保持一致。如果处于ArkUI开发环境中，采用框架默认像素单位vp。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_path.h
 *
 * @brief 文件中定义了与自定义路径相关的功能函数
 *
 * 引用文件"native_drawing/drawing_path.h"
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 添加闭合轮廓方向枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathDirection {
    /** 顺时针方向添加闭合轮廓 */
    PATH_DIRECTION_CW,
    /** 逆时针方向添加闭合轮廓 */
    PATH_DIRECTION_CCW,
} OH_Drawing_PathDirection;

/**
 * @brief 定义路径的填充类型枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathFillType {
    /** 绘制所有线段包围的区域 */
    PATH_FILL_TYPE_WINDING,
    /** 绘制所有线段包围奇次数的区域 */
    PATH_FILL_TYPE_EVEN_ODD,
    /** PATH_FILL_TYPE_WINDING 取反，绘制不在所有线段包围区域的点 */
    PATH_FILL_TYPE_INVERSE_WINDING,
    /** PATH_FILL_TYPE_EVEN_ODD 取反，绘制不在所有线段包围奇次数区域的点 */
    PATH_FILL_TYPE_INVERSE_EVEN_ODD,
} OH_Drawing_PathFillType;

/**
 * @brief 用于创建一个路径对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的路径对象。
 * @since 8
 * @version 1.0
 */
OH_Drawing_Path* OH_Drawing_PathCreate(void);

/**
 * @brief 创建一个路径对象副本{@link OH_Drawing_Path}，用于拷贝一个已有路径对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @return 函数返回一个指针，指针指向创建的路径对象副本{@link OH_Drawing_Path}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Path* OH_Drawing_PathCopy(OH_Drawing_Path*);

/**
 * @brief 用于销毁路径对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathDestroy(OH_Drawing_Path*);

/**
 * @brief 用于设置自定义路径的起始点位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param x 起始点的横坐标。
 * @param y 起始点的纵坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathMoveTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief 用于添加一条从路径的最后点位置到目标点位置的线段。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param x 目标点的横坐标。
 * @param y 目标点的纵坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathLineTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief 用于给路径添加一段弧线，绘制弧线的方式为角度弧，该方式首先会指定一个矩形边框，矩形边框会包裹椭圆，
 * 然后会指定一个起始角度和扫描度数，从起始角度扫描截取的椭圆周长一部分即为绘制的弧线。另外会默认添加一条从路径的最后点位置到弧线起始点位置的线段。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param x1 包围椭圆的矩形左上角点位置的横坐标。
 * @param y1 包围椭圆的矩形左上角点位置的纵坐标。
 * @param x2 包围椭圆的矩形右下角点位置的横坐标。
 * @param y2 包围椭圆的矩形右下角点位置的纵坐标。
 * @param startDeg 起始的角度。
 * @param sweepDeg 扫描的度数。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathArcTo(OH_Drawing_Path*, float x1, float y1, float x2, float y2, float startDeg, float sweepDeg);

/**
 * @brief 用于添加一条从路径最后点位置到目标点位置的二阶贝塞尔圆滑曲线。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param ctrlX 控制点位置的横坐标。
 * @param ctrlY 控制点位置的纵坐标。
 * @param endX 目标点位置的横坐标。
 * @param endY 目标点位置的纵坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathQuadTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY);

/**
 * @brief 用于添加一条从路径最后点位置到目标点位置的三阶贝塞尔圆滑曲线。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param ctrlX1 第一个控制点位置的横坐标。
 * @param ctrlY1 第一个控制点位置的纵坐标。
 * @param ctrlX2 第二个控制点位置的横坐标。
 * @param ctrlY2 第二个控制点位置的纵坐标。
 * @param endX 目标点位置的横坐标。
 * @param endY 目标点位置的纵坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathCubicTo(
    OH_Drawing_Path*, float ctrlX1, float ctrlY1, float ctrlX2, float ctrlY2, float endX, float endY);

/**
 * @brief 按指定方向，向路径添加矩形轮廓。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param left 矩形左上角的x轴坐标。
 * @param top 矩形左上角的y轴坐标。
 * @param right 矩形右下角的x轴坐标。
 * @param bottom 矩形右下角的y轴坐标。
 * @param OH_Drawing_PathDirection 路径方向{@link OH_Drawing_PathDirection}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRect(OH_Drawing_Path*, float left, float top,
    float right, float bottom, OH_Drawing_PathDirection);

/**
 * @brief 按指定方向，向路径添加圆角矩形轮廓。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_RoundRect 指向圆角矩形对象{@link OH_Drawing_RoundRect}的指针。
 * @param OH_Drawing_PathDirection 路径方向{@link OH_Drawing_PathDirection}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRoundRect(OH_Drawing_Path*, const OH_Drawing_RoundRect* roundRect, OH_Drawing_PathDirection);

/**
 * @brief 将弧添加到路径中，作为新轮廓的起点。从起始角度到扫掠角度添加弧，
 * 添加的弧是椭圆边界椭圆的一部分，单位为度。正扫掠表示按顺时针方向延长弧，
 * 负扫掠表示按逆时针方向延长弧。如果扫掠角度<= -360°，或扫掠角度>= 360°，
 * 并且起始角度对90取模接近于0，则添加椭圆而不是弧。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针。
 * @param startAngle 弧的起始角度，单位为度。
 * @param sweepAngle 扫掠角度，单位为度。正数表示顺时针方向，负数表示逆时针方向。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddArc(OH_Drawing_Path*, const OH_Drawing_Rect*, float startAngle, float sweepAngle);

/**
 * @brief 将原路径矩阵变换后，添加到当前路径中。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向当前路径对象{@link OH_Drawing_Path}的指针。
 * @param src 指向原路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPath(OH_Drawing_Path*, const OH_Drawing_Path* src, const OH_Drawing_Matrix*);

/**
 * @brief 按指定方向，向路径添加椭圆。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针。
 * @param OH_Drawing_PathDirection 路径方向{@link OH_Drawing_PathDirection}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddOval(OH_Drawing_Path*, const OH_Drawing_Rect*, OH_Drawing_PathDirection);

/**
 * @brief 判断指定坐标点是否被路径包含。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param x x轴上坐标点。
 * @param y y轴上坐标点。
 * @return 函数返回true表示点在路径内，函数返回false表示点在路径外。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathContains(OH_Drawing_Path*, float x, float y);

/**
 * @brief 对路径进行矩阵变换。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。 
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathTransform(OH_Drawing_Path*, const OH_Drawing_Matrix*);

/**
 * @brief 设置填充路径的规则。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_PathFillType 路径填充规则{@link OH_Drawing_PathFillType}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathSetFillType(OH_Drawing_Path*, OH_Drawing_PathFillType);


/**
 * @brief 用于闭合路径，会添加一条从路径起点位置到最后点位置的线段。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathClose(OH_Drawing_Path*);

/**
 * @brief 用于重置自定义路径数据。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathReset(OH_Drawing_Path*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif