/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_EventModule
 * @{
 *
 * @brief 在Native端提供ArkUI的UI输入事件能力。
 *
 * @since 12
 */

/**
 * @file ui_input_event.h
 *
 * @brief 提供ArkUI在Native侧的事件定义。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef UI_INPUT_EVENT
#define UI_INPUT_EVENT

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief UI输入事件定义。
 *
 * @since 12
 */
typedef struct ArkUI_UIInputEvent ArkUI_UIInputEvent;

/**
 * @brief UI输入事件类型定义。
 *
 * @since 12
 */
enum ArkUI_UIInputEvent_Type {
    ArkUI_UIINPUTEVENT_TYPE_UNKNOWN = 0,
    ArkUI_UIINPUTEVENT_TYPE_TOUCH = 1,
    ArkUI_UIINPUTEVENT_TYPE_AXIS = 2,
};

/**
 * @brief 获取UI输入事件的类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前UI输入事件的类型，如果参数异常则返回0。
 * @since 12
 */
int32_t OH_ArkUI_UIInputEvent_GetType(const ArkUI_UIInputEvent *event);

/**
 * @brief 获取UI输入事件发生的时间。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回UI输入事件发生的时间，如果参数异常则返回0。
 * @since 12
 */
int64_t OH_ArkUI_UIInputEvent_GetEventTime(const ArkUI_UIInputEvent *event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前组件左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetX(const ArkUI_UIInputEvent *event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前组件左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetY(const ArkUI_UIInputEvent *event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前应用窗口左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowX(const ArkUI_UIInputEvent *event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前应用窗口左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowY(const ArkUI_UIInputEvent *event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前屏幕左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayX(const ArkUI_UIInputEvent *event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前屏幕左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayY(const ArkUI_UIInputEvent *event);

/**
 * @brief 获取当前轴事件的垂直滚动轴的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的垂直滚动轴的值，如果参数异常则返回0.0。
 * @since 12
 */
double OH_ArkUI_AxisEvent_GetVerticalAxisValue(const ArkUI_UIInputEvent *event);

/**
 * @brief 获取当前轴事件的水平滚动轴的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的水平滚动轴的值，如果参数异常则返回0.0。
 * @since 12
 */
double OH_ArkUI_AxisEvent_GetHorizontalAxisValue(const ArkUI_UIInputEvent *event);

/**
 * @brief 获取当前轴事件的捏合轴缩放的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的捏合轴缩放的值，如果参数异常则返回0.0。
 * @since 12
 */
double OH_ArkUI_AxisEvent_GetPinchAxisScaleValue(const ArkUI_UIInputEvent *event);

#ifdef __cplusplus
};
#endif

#endif // UI_INPUT_EVENT
/** @} */
